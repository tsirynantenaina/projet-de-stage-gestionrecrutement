/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recrutement.requete.com;

/**
 *
 * @author Tsiry
 */
public class Requete {
    public String SQL;
    
    //identite maximum
    public String IDMax(String nomTable , String colonne){
        
        SQL=" SELECT max("+colonne+") FROM "+nomTable+" ";
        System.out.println(SQL);
        return (SQL);
    }
    
    //identite maximum
    public String Somme(String nomTable , String colonne){
        
        SQL=" SELECT sum("+colonne+") FROM "+nomTable+" ";
        System.out.println(SQL);
        return (SQL);
    }
    
    //identite maximum
    public String SommeAvecEtat(String nomTable , String colonne , String etat){
        
        SQL=" SELECT sum("+colonne+") FROM "+nomTable+" where "+etat+"";
        System.out.println(SQL);
        return (SQL);
    }
    
    //fonction  de requette qui affiche tous les element d'une table
    public String querySelectAll(String nomTable){
        
        SQL="SELECT * FROM "+nomTable+"";
        System.out.println(SQL);
        return (SQL);
    }
    
    //fonction de requette qui affiche les condition where avec "Ã©tat"
    public  String querySelectAll(String nomTable, String etat){       
        SQL="SELECT * FROM " + nomTable + " WHERE "+etat+" ";
        System.out.println(SQL);

        return (SQL);
    }
    
    
    //Colonne Specifié
    public String querySelect(String[] nomColonne, String nomTable){
        int i;
        SQL="SELECT";
            for(i=0 ; i<=nomColonne.length -1; i++){
                SQL +=nomColonne[i];
                if(i < nomColonne.length-1){
                    SQL +=",";
                }
            }
        SQL += "FROM" +nomTable+"";
        System.out.println(SQL);

        return(SQL);
    }
    
    
    //Colonne Specifié avec etat
    public String fcSelectCommand(String[] nomColonne, String nomTable, String etat){
       
        int i;
        SQL="SELECT";
            for(i=0  ;i <= nomColonne.length -1 ; i++){
                SQL +=nomColonne[i];
                if(i < nomColonne.length-1){
                    SQL +=",";
                }
            }
        SQL += "FROM" +nomTable+ "WHERE " + etat+""; 
        System.out.println(SQL);

        return (SQL);
    }
    
    //  Jointure avec nom colonne= table.nomColonne et etat -> table1.colonne = table2.colonne ..(and table3.colonne = table1.colone) 
    public String jointure(String[] nomColonne, String[] nomTable,String etat){
       
        int i;
        SQL="SELECT ";
            for(i=0  ;i <= nomColonne.length -1 ; i++){
                SQL +=nomColonne[i];
                if(i < nomColonne.length-1){
                    SQL +=" , ";
                }
            }
        SQL += " FROM ";for(i=0  ;i <= nomTable.length -1 ; i++){
                SQL +=nomTable[i];
                if(i < nomTable.length-1){
                    SQL +=" , ";
                }
            }
                
        SQL +=" WHERE "+etat+" "; 
        System.out.println(SQL);
        return (SQL);
    }
    
    

    
    
    // inserer des donnes dans le base de donnees
    public String queryInsert(String nomTable, String[] contenuTableau){
        int i;
        SQL = "INSERT INTO " + nomTable +" VALUES (";
            for(i=0 ; i <= contenuTableau.length -1 ; i++){
                SQL ="'" +contenuTableau[i]+"'";
                if(i < contenuTableau.length-1){
                    SQL +=",";
                }
            }
        SQL += ")";
                System.out.println(SQL);

        return(SQL);
        
    }
    
    //Inserer sur des colonnes bien specifiés
    public String queryInsert(String nomTable, String[] nomColonne, String[] contenuTableau){
        
        int i;
        SQL ="INSERT INTO "+ nomTable +" (";
            for(i=0 ; i <= nomColonne.length-1 ; i++ ){
                SQL += nomColonne[i];
                if(i <nomColonne.length -1){
                    SQL+=",";
                }
            }
  
        SQL +=") VALUES (";
            for(i=0 ; i <= contenuTableau.length -1 ; i++ ){
                SQL +="'"+contenuTableau[i]+"'";
                if (i < contenuTableau.length -1){
                    SQL+=",";
                }
            }
        SQL +=")";  
                System.out.println(SQL);

            return (SQL);
    }
    
    //modifier
     public String queryUpdate(String nomTable, String[] nomColonne, String[] contenuTableau,  String etat){
        int i;
        SQL ="UPDATE "+ nomTable +" SET ";
        
            for(i=0 ; i <= nomColonne.length-1 ; i++ ){
                SQL += ""+nomColonne[i]+"='"+contenuTableau[i]+"'";
                if(i < nomColonne.length -1){
                    SQL+=",";
                }
            }
  
        SQL += " WHERE "+etat;
                System.out.println(SQL);

        
        return(SQL);
    }
    
    //modifier
     public String queryUpdateStock(String nomTable, String[] nomColonne, String[] contenuTableau,  String etat){
        int i;
        SQL ="UPDATE "+ nomTable +" SET ";
        
            for(i=0 ; i <= nomColonne.length-1 ; i++ ){
                SQL += ""+nomColonne[i]+"="+contenuTableau[i]+"";
                if(i < nomColonne.length -1){
                    SQL+=",";
                }
            }
  
        SQL += " WHERE "+etat;
        System.out.println(SQL);
        return(SQL);
    }
     //fonction pour la requette supprimer sans parametre
    public String queryDelete(String nomTable){
        
        SQL = "DELETE FROM "+nomTable+"";
                System.out.println(SQL);

        return (SQL);
    }
    
    //fonction pour la requette suprimer avec parametre
    public String queryDelete(String nomTable, String etat){
       
        SQL = "DELETE FROM "+nomTable+" WHERE "+etat+"";
                System.out.println(SQL);

        return(SQL);
    }
    
}
