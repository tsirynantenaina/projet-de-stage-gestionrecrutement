/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recrutement.classe.com;


public class profils {
    private int idCandidtat ;
    private String profil ;

    public profils(int idCandidtat, String profil) {
        this.idCandidtat = idCandidtat;
        this.profil = profil;
    }

    public profils(String profil) {
        this.profil = profil;
    }

    public int getIdCandidtat() {
        return idCandidtat;
    }

    public void setIdCandidtat(int idCandidtat) {
        this.idCandidtat = idCandidtat;
    }

    public String getProfil() {
        return profil;
    }

    public void setProfil(String profil) {
        this.profil = profil;
    }
    
    
}
