/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recrutement.classe.com;

/**
 *
 * @author Tsiry
 */
public class Candidats {
    private int idCand ;
    private int idRec ;
    private String nom;
    private String prenom ; 
    private String dateNais ; 
    private String sexe;
    private String age ;
    private String cin ; 
    private String adrs ;
    private String tel;
    private String email;
    private String etat ;

    public Candidats(int idCand,int idRec, String nom, String prenom, String dateNais, String sexe, String age, String cin, String adrs, String tel , String email , String etat) {
        this.idCand = idCand;
        this.idRec = idRec;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNais = dateNais;
        this.sexe = sexe;
        this.age = age;
        this.cin = cin;
        this.adrs = adrs;
        this.tel = tel;
        this.email=email ;
        this.etat = etat ;
    }

    public Candidats(int idRec , String nom, String prenom, String dateNais, String sexe, String age, String cin, String adrs, String tel , String email ,String etat) {
        this.idRec = idRec;
        this.nom = nom;
        this.prenom = prenom;
        this.dateNais = dateNais;
        this.sexe = sexe;
        this.age = age;
        this.cin = cin;
        this.adrs = adrs;
        this.tel = tel;
        this.email= email ;
        this.etat = etat ;
    }

    public int getIdCand() {
        return idCand;
    }

    public int getIdRec() {
        return idRec;
    }

    public void setIdRec(int idRec) {
        this.idRec = idRec;
    }

    public void setIdCand(int idCand) {
        this.idCand = idCand;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getDateNais() {
        return dateNais;
    }

    public void setDateNais(String dateNais) {
        this.dateNais = dateNais;
    }

    public String getSexe() {
        return sexe;
    }

    public void setSexe(String sexe) {
        this.sexe = sexe;
    }

    public String getAge() {
        return age;
    }

    public void setAge(String age) {
        this.age = age;
    }

    public String getCin() {
        return cin;
    }

    public void setCin(String cin) {
        this.cin = cin;
    }

    public String getAdrs() {
        return adrs;
    }

    public void setAdrs(String adrs) {
        this.adrs = adrs;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getEtat() {
        return etat;
    }

    public void setEtat(String etat) {
        this.etat = etat;
    }
    
    
}


