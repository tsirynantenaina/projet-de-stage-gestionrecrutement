/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recrutement.classe.com;

/**
 *
 * @author Tsiry
 */
public class recru {
    private int idRec ;
    private int idCand ;
    private String poste ;
    private String types ;
    private String classe ;
    private String echelon ;
    private String indice ;
    private String date ;
    private String anneDeRecrutement;
    

    public recru(int idRec, int idCand, String poste , String types, String classe, String echelon, String indice , String date , String anneDeRecrutement) {
        this.idRec = idRec;
        this.idCand = idCand;
        this.poste = poste ;
        this.types = types;
        this.classe = classe;
        this.echelon = echelon;
        this.indice = indice;
        this.date = date;
        this.anneDeRecrutement = anneDeRecrutement;
    }

    public int getIdRec() {
        return idRec;
    }

    public void setIdRec(int idRec) {
        this.idRec = idRec;
    }

    public int getIdCand() {
        return idCand;
    }

    public String getPoste() {
        return poste;
    }

    public void setPoste(String poste) {
        this.poste = poste;
    }

    public void setIdCand(int idCand) {
        this.idCand = idCand;
    }

    public String getTypes() {
        return types;
    }

    public void setTypes(String types) {
        this.types = types;
    }

    public String getClasse() {
        return classe;
    }

    public void setClasse(String classe) {
        this.classe = classe;
    }

    public String getEchelon() {
        return echelon;
    }

    public void setEchelon(String echelon) {
        this.echelon = echelon;
    }

    public String getIndice() {
        return indice;
    }

    public void setIndice(String indice) {
        this.indice = indice;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public String getAnneDeRecrutement() {
        return anneDeRecrutement;
    }

    public void setAnneDeRecrutement(String anneDeRecrutement) {
        this.anneDeRecrutement = anneDeRecrutement;
    }
    
    
    
}
