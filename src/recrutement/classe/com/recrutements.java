/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recrutement.classe.com;

/**
 *
 * @author Tsiry
 */
public class recrutements {
    private int idRec ; 
    private String idSpecifique ; 
    private String avisRec ; 
    private String anneeRec;

    public recrutements(int idRec, String idSpecifique, String avisRec , String anneeRec) {
        this.idRec = idRec;
        this.idSpecifique = idSpecifique;
        this.avisRec = avisRec;
        this.anneeRec = anneeRec;
    }

    public recrutements(String idSpecifique, String avisRec, String anneeRec) {
        this.idSpecifique = idSpecifique;
        this.avisRec = avisRec;
        this.anneeRec = anneeRec;
    }

    public int getIdRec() {
        return idRec;
    }

    public void setIdRec(int idRec) {
        this.idRec = idRec;
    }

    public String getIdSpecifique() {
        return idSpecifique;
    }

    public void setIdSpecifique(String idSpecifique) {
        this.idSpecifique = idSpecifique;
    }

    public String getAvisRec() {
        return avisRec;
    }

    public void setAvisRec(String avisRec) {
        this.avisRec = avisRec;
    }

    public String getAnneeRec() {
        return anneeRec;
    }

    public void setAnneeRec(String anneeRec) {
        this.anneeRec = anneeRec;
    }
    
}
