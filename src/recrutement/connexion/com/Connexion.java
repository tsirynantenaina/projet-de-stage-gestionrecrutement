/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recrutement.connexion.com;

import com.mysql.jdbc.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Tsiry
 */
public class Connexion {
    
    public Connection con;
    public Statement st;
    
    public Connexion(){}
    
    public Connection Open(){
        try {
            Class.forName(Parametre.driv);
             System.out.println("le pilote jdbc:mysql est charger");
        } catch (ClassNotFoundException e) {
            System.out.println("Impossible de charger le pilote jdbc:mysql");
        }
        try {
            con = (Connection) DriverManager.getConnection(Parametre.url, Parametre.user, Parametre.password);
            st = con.createStatement();
            System.out.println("Connection reussie");
        } catch (SQLException e) {
            System.out.println("impossible de se connecter a la base de donne");
        }
        return con ;
    }
    

    public ResultSet executeQuery(String query) throws Exception {
        ResultSet rs = null;
        rs = st.executeQuery(query);
        return (rs);
    }

    public int executeUpdate(String query) throws Exception {
        int rs = 0;
        rs = st.executeUpdate(query);
        return (rs);
    }

    public void close() throws Exception {
        st.close();
        con.close();
    }
    
}
