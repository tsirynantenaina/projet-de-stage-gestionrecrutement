/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recrutement.gestion.com;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.table.DefaultTableModel;
import recrutement.classe.com.recrutements;
import recrutement.connexion.com.Connexion;
import recrutement.requete.com.Requete;

/**
 *
 * @author Tsiry
 */
public class gestionRecrutement {
    public Connexion con = new Connexion();
    public PreparedStatement ps;
    public ResultSet rs;
    public String txt ="";
    
    public gestionRecrutement (){
    }
    
    public void insererRecrutement(recrutements r){
        String req = "insert into recrutement(idSpecifique,avisRec,anneeRec) values(?,?,?)";
        try {
            ps = (PreparedStatement) con.Open().prepareStatement(req);
            ps.setString(1,r.getIdSpecifique());
            ps.setString(2,r.getAvisRec());
            ps.setString(3,r.getAnneeRec());
            ps.execute();
        }catch(Exception e){
            System.err.println(e.toString());
            
        } 
        
    }
    
    public void modifierRecrutement(recrutements r){
       String req = "update recrutement set idSpecifique = ? , avisRec = ? , anneeRec = ? where idRec =?";
        try {
            ps = (PreparedStatement)con.Open().prepareStatement(req);
            ps.setString(1,r.getIdSpecifique());
            ps.setString(2,r.getAvisRec());
            ps.setString(3,r.getAnneeRec());
            ps.setString(4,String.valueOf(r.getIdRec()));
            ps.execute();
        }catch(Exception e){
            System.err.println(e.toString());
            
        } 
    }
    
    public void afficherModification(javax.swing.JLabel annee ,javax.swing.JLabel titre , javax.swing.JTextArea avis , javax.swing.JLabel idRec ){
        try {
            String req = "select * from recrutement where idRec='"+idRec.getText()+"'";
            rs = execution(req);
            if(rs.next()) {                
                annee.setText(rs.getString("anneeRec"));
                titre.setText(rs.getString("idSpecifique"));
                 int i;
                 txt="";
                 String [] tab = rs.getString("avisRec").split("#");
                 for(i=0 ; i<=tab.length -1; i++){
                     txt +=tab[i];
                    if(i < tab.length-1){
                       txt +="'";
                   }
                }
                txt += "";
                avis.setText(txt);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
    
    
   
    
    
    public void delete(recrutements r){
        String req = "delete from recrutement where idRec=?";
        try {
            ps = con.Open().prepareStatement(req);
            ps.setString(1,String.valueOf(r.getIdRec()));
            ps.execute();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
    public ResultSet execution ( String req ){
        try {
            ps = (PreparedStatement) con.Open().prepareStatement(req);
            rs = ps.executeQuery(req);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return rs;
    }
    
      public void listeRecrutement(javax.swing.JTable tbl){
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("ID");
        model.addColumn("Titre");
        model.addColumn("Avis"); 
        model.addColumn("Année");
        tbl.setModel(model);
        String req= "select * from recrutement order by idRec DESC";
        try {
            rs = execution(req);
            while (rs.next()) {                
                String id= rs.getString("idRec");
                String titre= rs.getString("idSpecifique");
                String avis= rs.getString("avisRec");
                String annee= rs.getString("anneeRec");
                Object[] ligne = {id, titre , avis , annee};
                model.addRow(ligne);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
      public void rechercher(javax.swing.JTable tbl , String mot){
        DefaultTableModel model = new DefaultTableModel();
        model.setRowCount(0);
        model.addColumn("ID");
        model.addColumn("Titre");
         model.addColumn("Avis");
         model.addColumn("Année");
        tbl.setModel(model);
        String req= "Select * from recrutement where idRec like '%"+mot+"%' or "
                + "idSpecifique like '%"+mot+"%' or anneeRec like '%"+mot+"%'"
                + " order by idRec DESC";
        try {
            rs = execution(req);
            while (rs.next()) {                
                String id= rs.getString("idRec");
                String titre= rs.getString("idSpecifique");
                String avis= rs.getString("avisRec");
                String annee= rs.getString("anneeRec");
                Object[] ligne = {id, titre , avis , annee};
                model.addRow(ligne);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
      }
      
     public void actualiser(javax.swing.JTable table){
        DefaultTableModel model = new DefaultTableModel();
        model.setRowCount(0);
        listeRecrutement(table);
        
    }
    
     public void rechercheAvantSuppr(String idRec , javax.swing.JLabel labl){
         String req= "select * from candidat where idRec = "+idRec+"";
        try {
            rs = execution(req);
            if(rs.next()) { 
                labl.setText(rs.getString("idRec"));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
     }
    
    
}
