/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recrutement.gestion.com;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import recrutement.classe.com.Candidats;
import recrutement.classe.com.profils;
import recrutement.connexion.com.Connexion;

/**
 *
 * @author Tsiry
 */
public class gestionProfil {
    public Connexion con = new Connexion();
    public PreparedStatement ps;
    public ResultSet rs ;
    public String txt="" ;
    public String txtmdf="";
    
    public gestionProfil(){}
    
    public void insererProfil(profils p){
        String req = "insert into profil(idCand , profil) values (? , ? ) ";
        try {
            ps = (PreparedStatement) con.Open().prepareStatement(req);
            ps.setString(1,String.valueOf(p.getIdCandidtat()));
            ps.setString(2,p.getProfil());
            ps.execute();
        }catch(Exception e){
            System.err.println(e.toString());
            
        }
    }
    
    public void modifierProfil(profils p){
        String req = "update profil set profil = ? where idCand = ?  ";
        try {
            ps = (PreparedStatement) con.Open().prepareStatement(req);
            ps.setString(2,String.valueOf(p.getIdCandidtat()));
            ps.setString(1,p.getProfil());
            ps.execute();
        }catch(Exception e){
            System.err.println(e.toString());
            
        }
    }
     public ResultSet execution ( String req ){
        try {
            ps = (PreparedStatement) con.Open().prepareStatement(req);
            rs = ps.executeQuery(req);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return rs;
    }
    
    public void idCandidat(javax.swing.JLabel id){
        String req="select max(idCand) as max from candidat";
        try {
            rs = execution(req);
            if(rs.next()) {                
              id.setText(rs.getString("max"));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
    public void profilCandidat(javax.swing.JTextArea txt , String idCand){
        String req="select profil  from profil where idCand= "+idCand+"";
        try {
            rs = execution(req);
            if(rs.next()) {                
              txt.setText(rs.getString("profil"));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
    public void deplacement(String idCand , javax.swing.JTextArea profil){
        String req= "select * from profil where idCand='"+idCand+"'";
        try {
            rs = execution(req);
            if(rs.next()) { 
                int i;
                 txt="";
                 String [] tab = rs.getString("profil").split("#");
                 for(i=0 ; i<=tab.length -1; i++){
                     txt +=tab[i];
                    if(i < tab.length-1){
                       txt +="'";
                   }
                }
                txt += "";
                profil.setText(txt);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
    }
    
     public void afficherModification(String idCand , javax.swing.JTextArea profil){
        String req= "select * from profil where idCand='"+idCand+"'";
        try {
            rs = execution(req);
            if(rs.next()) {
               int i;
                 txtmdf="";
                 String [] tab = rs.getString("profil").split("#");
                 for(i=0 ; i<=tab.length -1; i++){
                     txtmdf +=tab[i];
                    if(i < tab.length-1){
                       txtmdf +="'";
                   }
                }
                txtmdf += "";
                profil.setText(txtmdf);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
    }
     
     
    public void delete(String id){
        String req = "delete from profil where idCand=?";
        try {
            ps = con.Open().prepareStatement(req);
            ps.setString(1,id);
            ps.execute();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
    public void deletePerfo(String idC){
        String req = "delete from performance where idCand=?";
        try {
            ps = con.Open().prepareStatement(req);
            ps.setString(1,idC);
            ps.execute();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
    
    public void insererPefro(String idCand , String exp ,String spec ,String niv ,String frns , String angls , String diplo , int perf){
        String req = "insert into performance(idCand , txtExp , txtNiv , txtSpecialite ,txtLangueFrns ,txtLangueAngls , txtDiplome , performance ) value(?,? , ? ,? , ?, ? , ? , ?)";
        try {
            ps = con.Open().prepareStatement(req);
            ps.setString(1,idCand);
            ps.setString(2,exp);
            ps.setString(3,spec);
            ps.setString(4,niv);
            ps.setString(5,frns);
            ps.setString(6,angls);
            ps.setString(7,diplo);
            ps.setInt(8,perf);
            ps.execute();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
    
    public void affichagePerfo(String idCand, javax.swing.JLabel txtExp , javax.swing.JLabel txtNiv , javax.swing.JLabel txtSpecialite , 
            javax.swing.JLabel txtFrns ,javax.swing.JLabel txtAngls , javax.swing.JLabel txtDiplo , javax.swing.JLabel performance) {
        String req="select *  from performance where idCand= "+idCand+"";
        try {
            rs = execution(req);
            if(rs.next()) {                
              txtExp.setText(rs.getString("txtExp"));
              txtNiv.setText(rs.getString("txtNiv"));
              txtSpecialite.setText(rs.getString("txtSpecialite"));
              txtFrns.setText(rs.getString("txtLangueFrns"));
              txtAngls.setText(rs.getString("txtLangueAngls"));
              txtDiplo.setText(rs.getString("txtDiplome"));
              performance.setText(""+rs.getString("performance")+"%");
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
   public void updatePefro(String idCand , String exp ,String spec ,String niv ,String frns , String angls , String diplo , int perf){
        String req = "update   performance set  txtExp=? , txtNiv=? , txtSpecialite=? ,txtLangueFrns=? ,txtLangueAngls=? , txtDiplome=? , performance=? where idCand = ?";
        try {
            ps = con.Open().prepareStatement(req);
            ps.setString(1,exp);
            ps.setString(2,spec);
            ps.setString(3,niv);
            ps.setString(4,frns);
            ps.setString(5,angls);
            ps.setString(6,diplo);
            ps.setInt(7,perf);
             ps.setString(8,idCand);
            ps.execute();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
    
    
}
