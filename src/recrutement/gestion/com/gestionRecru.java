/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recrutement.gestion.com;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import javax.swing.table.DefaultTableModel;
import recrutement.classe.com.profils;
import recrutement.classe.com.recru;
import recrutement.connexion.com.Connexion;

/**
 *
 * @author Tsiry
 */
public class gestionRecru {
     public Connexion con = new Connexion();
    public PreparedStatement ps;
    public ResultSet rs ;
   
    
    public gestionRecru(){}
    
    public void insererRecru(recru r){
        String req = "insert into recru (idRec, idCand ,poste,types , classe , echelon , indice , dateRecru , anneDeRecrutement) values (? ,? , ? ,? , ? , ? ,?, ? , ?) ";
        try {
            ps = (PreparedStatement) con.Open().prepareStatement(req);
            ps.setString(1,String.valueOf(r.getIdRec()));
            ps.setString(2,String.valueOf(r.getIdCand()));
             ps.setString(3,r.getPoste());
            ps.setString(4,r.getTypes());
            ps.setString(5,r.getClasse());
            ps.setString(6,r.getEchelon());
            ps.setString(7,r.getIndice());
            ps.setString(8,r.getDate()); 
            ps.setString(9,r.getAnneDeRecrutement());
            
            ps.execute();
        }catch(Exception e){
            System.err.println(e.toString());
            
        }
    }
    
    public void UpdateEtat(String id){
        String req = "update candidat set validation = 'recruté' where idCand = ? ";
        try {
            ps = (PreparedStatement) con.Open().prepareStatement(req);
            ps.setString(1,id);
            ps.execute();
        }catch(Exception e){
            System.err.println(e.toString());
            
        }
    }
    
    
     public ResultSet execution ( String req ){
        try {
            ps = (PreparedStatement) con.Open().prepareStatement(req);
            rs = ps.executeQuery(req);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return rs;
    }
    public void liste(javax.swing.JTable tbl , javax.swing.JLabel idRec ){
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("rercutement");
        model.addColumn("année");
        model.addColumn("identité");
        model.addColumn("nom");
        model.addColumn("prenom"); 
        model.addColumn("poste"); 
        model.addColumn("types");
        model.addColumn("classe");
        model.addColumn("echelon");
        model.addColumn("indice");
         model.addColumn("date");
        tbl.setModel(model);
        String req="select recrutement.idSpecifique , recrutement.anneeRec , candidat.idCand , candidat.nomCand , candidat.prenomCand ,"
                + "recru.poste , recru.types , recru.classe , recru.echelon , recru.indice , recru.dateRecru from recrutement , candidat , recru "
                + "where  candidat.idCand = recru.idCand and recrutement.idRec = recru.idRec and recru.idRec= '"+idRec.getText()+"'";
        try {
            rs =execution(req);
            while (rs.next()) {                
                String id= rs.getString("idSpecifique");
                String annee= rs.getString("anneeRec");
                String idCa= rs.getString("idCand");
                String nom =  rs.getString("nomCand");
                String prenom =  rs.getString("prenomCand");
                String poste =  rs.getString("poste");
                String types=  rs.getString("types");
                String classe =  rs.getString("classe");
                String echelon =  rs.getString("echelon");
                String indice =  rs.getString("indice");
                String dt =rs.getString("dateRecru");
                Object[] ligne = {id, annee , idCa , nom , prenom , poste , types , classe , echelon , indice, dt };
                model.addRow(ligne);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
     public void listeCacher(javax.swing.JTable tbl , javax.swing.JLabel idRec ){
       DefaultTableModel md= new DefaultTableModel();
       md.addColumn("identite du Candidat");
       md.addColumn("nom");
       md.addColumn("prenom");
       
        
        tbl.setModel(md);
        String req="select recrutement.idSpecifique , recrutement.anneeRec , candidat.idCand , candidat.nomCand , candidat.prenomCand ,"
                + "recru.poste , recru.types , recru.classe , recru.echelon , recru.indice , recru.dateRecru from recrutement , candidat , recru "
                + "where  candidat.idCand = recru.idCand and recrutement.idRec = recru.idRec and recru.idRec= '"+idRec.getText()+"'";
        try {
            rs =execution(req);
            while (rs.next()) {                
                String nom =  rs.getString("nomCand");
                String prenom =  rs.getString("prenomCand");
                String idCa= rs.getString("idCand");
                
                Object[] ligne = { idCa , nom , prenom };
                md.addRow(ligne);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
     
     
    public void actualiser(javax.swing.JTable table  ,javax.swing.JLabel idRec  ){
        DefaultTableModel model = new DefaultTableModel();
        model.setRowCount(0);
        liste(table , idRec  );
        
    } 
     
    public void delete(String id){
        String req = "delete from recru where idCand=?";
        try {
            ps = con.Open().prepareStatement(req);
            ps.setString(1,id);
            ps.execute();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
    public void update(String id){
        String req = "update candidat set validation ='non recruté' where idCand=?";
        try {
            ps = con.Open().prepareStatement(req);
            ps.setString(1,id);
            ps.execute();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
    
    public void getAnnneDeRec( javax.swing.JLabel labelAnne , String idRec){
        String req ="select anneeRec from recrutement where idRec = '"+idRec+"'";
        try {
            rs =execution(req);
            if(rs.next()) {
                labelAnne.setText(rs.getString("anneeRec"));
            }
        }catch (Exception e) {
            System.err.println(e.toString());
        }
            
    }
    
    public void compte(String idRec ,javax.swing.JLabel labelValeur){
        try {
            String req = "select count(*) as compte from recru where idRec='"+idRec+"'";
            rs = execution(req);
            if(rs.next()) { 
              labelValeur.setText(rs.getString("compte"));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
    
}
