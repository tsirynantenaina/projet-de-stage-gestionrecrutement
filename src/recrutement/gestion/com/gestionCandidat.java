/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recrutement.gestion.com;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.ImageIcon;
import javax.swing.table.DefaultTableModel;
import recrutement.classe.com.Candidats;
import recrutement.classe.com.recrutements;
import recrutement.connexion.com.Connexion;
import recrutement.requete.com.Requete;

/**
 *
 * @author Tsiry
 */
public class gestionCandidat {
    public Connexion con = new Connexion();
    public PreparedStatement ps;
    public ResultSet rs ;
    public gestionCandidat(){}
    
    public void insererCandidat(Candidats c ,  byte[] photo ){
        String req = "insert into candidat(idRec , nomCand , prenomCand , dateNaissCand , sexeCand , ageCand , CINCand , adrsCand , telCand , emailCand , image ,validation) values(?,?,?,?,?,?,?,?,?,?,?,?)";
        try {
            ps = (PreparedStatement) con.Open().prepareStatement(req);
            ps.setString(1,String.valueOf(c.getIdRec()));
            ps.setString(2,c.getNom());
            ps.setString(3,c.getPrenom());
            ps.setString(4,c.getDateNais());
            ps.setString(5,c.getSexe());
            ps.setString(6,c.getAge());
            ps.setString(7,c.getCin());
            ps.setString(8,c.getAdrs());
            ps.setString(9,c.getTel());
            ps.setString(10,c.getEmail());
            ps.setBytes(11 , photo);
            ps.setString(12 ,c.getEtat());
            ps.execute();
        }catch(Exception e){
            System.err.println(e.toString());
            
        }
    }
    
    public void modifierCandidatAvecImage(Candidats c , byte[] photo ){
        String req = "update candidat set idRec=? , nomCand=? , prenomCand=? , dateNaissCand=? , sexeCand=? , ageCand=? , CINCand=? , adrsCand=? , telCand =? ,emailCand =? , image = ? where idCand = ?";
        try {
            ps = (PreparedStatement) con.Open().prepareStatement(req);
            ps.setString(1,String.valueOf(c.getIdRec()));
            ps.setString(2,c.getNom());
            ps.setString(3,c.getPrenom());
            ps.setString(4,c.getDateNais());
            ps.setString(5,c.getSexe());
            ps.setString(6,c.getAge());
            ps.setString(7,c.getCin());
            ps.setString(8,c.getAdrs());
            ps.setString(9,c.getTel());
            ps.setString(10,c.getTel());
            ps.setBytes(11 , photo);
            ps.setString(12,String.valueOf(c.getIdCand()));
            ps.execute();
        }catch(Exception e){
            System.err.println(e.toString());
            System.err.println(req);
            
        }
    }
    public void modifierCandidatSansImage(Candidats c){
        String req = "update candidat set idRec=? , nomCand=? , prenomCand=? , dateNaissCand=? , sexeCand=? , ageCand=? , CINCand=? , adrsCand=? , telCand =? , emailCand =?  where idCand = ?";
        try {
            ps = (PreparedStatement) con.Open().prepareStatement(req);
            ps.setString(1,String.valueOf(c.getIdRec()));
            ps.setString(2,c.getNom());
            ps.setString(3,c.getPrenom());
            ps.setString(4,c.getDateNais());
            ps.setString(5,c.getSexe());
            ps.setString(6,c.getAge());
            ps.setString(7,c.getCin());
            ps.setString(8,c.getAdrs());
            ps.setString(9,c.getTel());
            ps.setString(10,c.getEmail());
            ps.setString(11,String.valueOf(c.getIdCand()));
            ps.execute();
        }catch(Exception e){
            System.err.println(e.toString());
             System.err.println(req);
            
        }
    }
    
    public void delete(String idCand){
        String req = "delete from candidat where idCand=?";
        try {
            ps = con.Open().prepareStatement(req);
            ps.setString(1,idCand);
            ps.execute();
        } catch (Exception e) {
            System.err.println(e.toString());
        }
    }
    public ResultSet execution ( String req ){
        try {
            ps = (PreparedStatement) con.Open().prepareStatement(req);
            rs = ps.executeQuery(req);
        } catch (Exception e) {
            System.out.println(e.toString());
        }
        return rs;
    }
    
    public void combo(javax.swing.JComboBox comboBox){
        String req="select * from recrutement order by idRec Desc";
        try {
            rs = execution(req);
            while (rs.next()) {                
                comboBox.addItem(rs.getString("idSpecifique"));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
    public void idRec(javax.swing.JComboBox comboBox ,javax.swing.JLabel txt  ){
        String req= "select * from recrutement where idSpecifique='"+comboBox.getSelectedItem().toString()+"'";
        try {
             rs = execution(req);
            if(rs.next()) {                
                txt.setText(rs.getString("idRec"));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
     public void listeCandidat(javax.swing.JTable tbl , javax.swing.JLabel idRec  ){
        DefaultTableModel model = new DefaultTableModel();
        model.addColumn("ID");
        model.addColumn("nom");
        model.addColumn("Prenom"); 
        model.addColumn("Validation"); 
        tbl.setModel(model);
        String req="select * from candidat where IdRec="+idRec.getText()+"";
        try {
            rs =execution(req);
            while (rs.next()) {                
                String id= rs.getString("idCand");
                String nom= rs.getString("nomCand");
                String prenom= rs.getString("prenomCand");
                String val= rs.getString("validation");
                Object[] ligne = {id, nom , prenom ,val };
                model.addRow(ligne);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
      public void rechercher(javax.swing.JTable tbl ,  String idRec , String mot){
        DefaultTableModel model = new DefaultTableModel();
        model.setRowCount(0);
        model.addColumn("ID");
        model.addColumn("nom");
        model.addColumn("Prenom"); 
        tbl.setModel(model);
        String req= "select * from candidat where nomCand like '%"+mot+"%' and idRec = "+idRec+"";
        try {
            rs = execution(req);
            while (rs.next()) {                
                String id= rs.getString("idCand");
                String nom= rs.getString("nomCand");
                String prenom= rs.getString("prenomCand");
                Object[] ligne = {id, nom , prenom };
                model.addRow(ligne);
            }
        } catch (Exception e) {
            System.err.println(e);
        }
      }
      
    public void image(String idCand , javax.swing.JLabel image , ImageIcon Format ){
        String req= "select * from candidat where idCand='"+idCand+"'";
        try {
            rs = execution(req);
            if(rs.next()) { 
               if(rs.getBytes("image")!=null){
                    byte[] img = rs.getBytes("image");
                    Format = new ImageIcon(img);
                    image.setIcon(Format);
               }else{
                   image.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/default.PNG")));
               }
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    
    }
    
    public void deplacement(String idCand , javax.swing.JComboBox combo , javax.swing.JLabel id ,  javax.swing.JLabel nm , javax.swing.JLabel prenom , javax.swing.JLabel dateNais ,
            javax.swing.JLabel sexe , javax.swing.JLabel age , javax.swing.JLabel cin , javax.swing.JLabel adrs , javax.swing.JLabel tel, javax.swing.JLabel email , javax.swing.JLabel val ){
        String req= "select * from candidat where idCand='"+idCand+"'";
        try {
            rs = execution(req);
            if(rs.next()) { 
            
             id.setText(rs.getString("idCand"));
             nm.setText(rs.getString("nomCand"));
             prenom.setText(rs.getString("prenomCand"));
             dateNais.setText(rs.getString("dateNaissCand"));
             sexe.setText(rs.getString("sexeCand"));
             age.setText(rs.getString("ageCand"));
             cin.setText(rs.getString("CINCand"));
             adrs.setText(rs.getString("adrsCand"));
             tel.setText(rs.getString("telCand"));
             email.setText(rs.getString("emailCand"));
             val.setText(rs.getString("validation"));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
        
    }
    
    
      
     public void actualiser(javax.swing.JTable table  ,javax.swing.JLabel idRec  ){
        DefaultTableModel model = new DefaultTableModel();
        model.setRowCount(0);
        listeCandidat(table , idRec  );
        
    }
     
    public void afficherModification(javax.swing.JLabel nom,javax.swing.JLabel prenom , javax.swing.JLabel dateNaiss , javax.swing.JLabel sexe ,  javax.swing.JLabel age ,  javax.swing.JLabel CIN ,  javax.swing.JLabel adrs ,  javax.swing.JLabel tel ,  javax.swing.JLabel idCand , javax.swing.JLabel mail , javax.swing.JLabel val){
        try {
            String req = "select * from candidat where idCand='"+idCand.getText()+"'";
            rs = execution(req);
            if(rs.next()) { 
                nom.setText(rs.getString("nomCand"));
                prenom.setText(rs.getString("prenomCand"));
                dateNaiss.setText(rs.getString("dateNaissCand"));
                sexe.setText(rs.getString("sexeCand"));
                age.setText(rs.getString("ageCand"));
                CIN.setText(rs.getString("CINCand"));
                adrs.setText(rs.getString("adrsCand"));
                tel.setText(rs.getString("telCand"));
                mail.setText(rs.getString("emailCand"));
                val.setText(rs.getString("validation"));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
    public void idRecApresModification(String idCand ,javax.swing.JLabel idRecApres){
        try {
            String req = "select * from candidat where idCand='"+idCand+"'";
            rs = execution(req);
            if(rs.next()) { 
                idRecApres.setText(rs.getString("idRec"));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
    
    public void compte(String idRec ,javax.swing.JLabel labelValeur){
        try {
            String req = "select count(*) as compte from candidat where idRec='"+idRec+"'";
            rs = execution(req);
            if(rs.next()) { 
              labelValeur.setText(rs.getString("compte"));
            }
        } catch (Exception e) {
            System.err.println(e);
        }
    }
}

