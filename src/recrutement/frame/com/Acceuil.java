/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor. 
 */
package recrutement.frame.com;

import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.awt.Color;
import java.beans.PropertyVetoException;
import static java.lang.Thread.sleep;
import java.util.Calendar;
import java.util.GregorianCalendar;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.jfree.chart.ChartFactory;
import org.jfree.chart.ChartFrame;
import org.jfree.chart.ChartPanel;
import org.jfree.chart.JFreeChart;
import org.jfree.chart.plot.CategoryPlot;
import org.jfree.chart.plot.PlotOrientation;
import org.jfree.data.category.DefaultCategoryDataset;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import java.awt.event.KeyEvent;
import static java.lang.Thread.sleep;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import recrutement.classe.com.recrutements;
import recrutement.gestion.com.gestionRecrutement;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import static java.lang.Thread.sleep;
import static com.sun.org.apache.xalan.internal.lib.ExsltDatetime.date;
import static java.lang.Thread.sleep;
import static sun.security.jgss.GSSUtil.login;

/**
 *
 * @author Tsiry
 */
public class Acceuil extends javax.swing.JFrame {
    public gestionRecrutement gr = new gestionRecrutement();
    public Acceuil() {
        initComponents();
        date();
        heure();   
        Graphe();
        invisibleDeconnecte();
 
        
    }
    public void user(String nomUser){
        user.setText(nomUser);
    }
     public void heure(){
       Thread clock = new Thread(){
           public void run(){
               for(;;){
                   Calendar Cal=new GregorianCalendar();
                   int s=Cal.get(Calendar.SECOND);
                   int m=Cal.get(Calendar.MINUTE);
                   int h=Cal.get(Calendar.HOUR);
                   int AM_PM=Cal.get(Calendar.AM_PM);
                   String val;
                   int a;
                   if(AM_PM==1){
                       val="PM";
                       a=12;
                   }else{
                       val="AM";
                       a=00;
                   }
                   int horloge = h + a;
                   if(horloge < 10 && m <10){
                    heure.setText("0"+horloge+":0"+m+"");
                   }else if(horloge <10 && m >10){
                     heure.setText("0"+horloge+":"+m+"");
                   }else if(horloge >10 && m <10){
                     heure.setText(""+horloge+":0"+m+"");
                   }else{
                     heure.setText(""+horloge+":"+m+"");
                   }
                   
                   try{
                       sleep(1000);
                   }catch(InterruptedException ex){
                        Logger.getLogger(Calendar.class.getName()).log(Level.SEVERE, null,ex);
                   }
               }
           }
       };
       clock.start();
   }
      
       public void date(){
       Thread clock = new Thread(){
           public void run(){
               for(;;){
                   Calendar Cal=new GregorianCalendar();
                   int annee=Cal.get(Calendar.YEAR);
                   int mois=Cal.get(Calendar.MONTH);
                   int jour=Cal.get(Calendar.DAY_OF_MONTH);
                   if(jour<10 && mois <10 ){      
                   date.setText("Aujourd'hui : 0"+jour+"-0"+(mois+1)+"-"+annee+" ");
                   }
                   else if(jour<10 && mois >10 ){      
                    date.setText("Aujourd'hui : 0"+jour+"-"+(mois+1)+"-"+annee+" ");
                   }
                   else if(jour>10 && mois <10 ){      
                    date.setText("Aujourd'hui : "+jour+"-0"+(mois+1)+"-"+annee+" ");
                   }else{
                       date.setText("Aujourd'hui : "+jour+"-"+(mois+1)+"-"+annee+" ");
                   }
                   try{
                       sleep(1000);
                   }catch(InterruptedException ex){
                        Logger.getLogger(Calendar.class.getName()).log(Level.SEVERE, null,ex);
                   }
               }
           }
       };
       clock.start();
   }
       

  
    
 
    
    public void Graphe(){
        this.panelPrincipal.removeAll();
        this.panelPrincipal.repaint();     
        statistique r = new statistique();
        this.panelPrincipal.add(r);
        try {
            r.setMaximum(true);
        } catch (Exception ex) {
            System.out.println(ex);
        }
        r.show();
      
        
    }
    
    public void invisibleDeconnecte(){
        labelDeconnect.setVisible(false);creer.setVisible(false);chagerCompte.setVisible(false);deconnecter.setVisible(false);
    }
    
    public void visibleDeconnecte(){
        labelDeconnect.setVisible(true);creer.setVisible(true);chagerCompte.setVisible(true);deconnecter.setVisible(true);
    }
    
   
    
   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        deconnecter = new javax.swing.JLabel();
        chagerCompte = new javax.swing.JLabel();
        creer = new javax.swing.JLabel();
        labelDeconnect = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        user = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        visiter6 = new javax.swing.JLabel();
        jLabel23 = new javax.swing.JLabel();
        jLabel22 = new javax.swing.JLabel();
        jLabel21 = new javax.swing.JLabel();
        visiter5 = new javax.swing.JLabel();
        visiter4 = new javax.swing.JLabel();
        visiter3 = new javax.swing.JLabel();
        visiter2 = new javax.swing.JLabel();
        jLabel20 = new javax.swing.JLabel();
        jLabel19 = new javax.swing.JLabel();
        jLabel18 = new javax.swing.JLabel();
        jLabel17 = new javax.swing.JLabel();
        jLabel16 = new javax.swing.JLabel();
        date = new javax.swing.JLabel();
        heure = new javax.swing.JLabel();
        jLabel15 = new javax.swing.JLabel();
        jLabel13 = new javax.swing.JLabel();
        jLabel12 = new javax.swing.JLabel();
        jLabel8 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        jLabel10 = new javax.swing.JLabel();
        jLabel7 = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel46 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        SousPanelPrincipal = new javax.swing.JPanel();
        panelPrincipal = new javax.swing.JPanel();
        jLabel3 = new javax.swing.JLabel();
        jLabel25 = new javax.swing.JLabel();
        jLabel24 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jLabel2 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        form2 = new javax.swing.JLabel();
        messageDec = new javax.swing.JLabel();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        setUndecorated(true);
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                formMouseEntered1(evt);
            }
        });
        addWindowListener(new java.awt.event.WindowAdapter() {
            public void windowActivated(java.awt.event.WindowEvent evt) {
                formWindowActivated(evt);
            }
            public void windowOpened(java.awt.event.WindowEvent evt) {
                formWindowOpened(evt);
            }
        });
        getContentPane().setLayout(new org.netbeans.lib.awtextra.AbsoluteLayout());

        deconnecter.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/deconnecter.PNG"))); // NOI18N
        deconnecter.setText("jLabel28");
        deconnecter.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        deconnecter.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                deconnecterMouseClicked(evt);
            }
        });
        getContentPane().add(deconnecter, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 104, 130, 20));

        chagerCompte.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/changer.PNG"))); // NOI18N
        chagerCompte.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        chagerCompte.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                chagerCompteMouseClicked(evt);
            }
        });
        getContentPane().add(chagerCompte, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 80, 140, 20));

        creer.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/userplus.PNG"))); // NOI18N
        creer.setText("jLabel26");
        creer.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        creer.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                creerMouseClicked(evt);
            }
        });
        getContentPane().add(creer, new org.netbeans.lib.awtextra.AbsoluteConstraints(1180, 50, 140, 20));

        labelDeconnect.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        labelDeconnect.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/dec.PNG"))); // NOI18N
        labelDeconnect.setBorder(javax.swing.BorderFactory.createTitledBorder(""));
        getContentPane().add(labelDeconnect, new org.netbeans.lib.awtextra.AbsoluteConstraints(1170, 40, 170, 100));

        jPanel1.setBackground(new java.awt.Color(33, 150, 243));

        user.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        user.setForeground(new java.awt.Color(255, 255, 255));
        user.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        user.setText("Administrateur");

        jLabel5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/Capture_1.PNG"))); // NOI18N
        jLabel5.setText("jLabel5");
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel5MouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap(31, Short.MAX_VALUE)
                .addComponent(user, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 49, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap())
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                .addComponent(jLabel5, javax.swing.GroupLayout.PREFERRED_SIZE, 50, Short.MAX_VALUE)
                .addComponent(user, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE))
        );

        getContentPane().add(jPanel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(1090, 5, 250, 50));

        visiter6.setBackground(new java.awt.Color(153, 153, 153));
        visiter6.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        visiter6.setForeground(new java.awt.Color(153, 153, 153));
        visiter6.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        visiter6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG"))); // NOI18N
        visiter6.setText("  Reservation");
        visiter6.setAlignmentX(10.0F);
        visiter6.setAlignmentY(10.0F);
        visiter6.setMinimumSize(new java.awt.Dimension(1473, 500));
        getContentPane().add(visiter6, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 540, 10, 50));

        jLabel23.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel23.setForeground(new java.awt.Color(153, 153, 153));
        jLabel23.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel23.setAlignmentX(10.0F);
        jLabel23.setAlignmentY(10.0F);
        jLabel23.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel23.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel23MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel23, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 540, 200, 50));

        jLabel22.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel22.setForeground(new java.awt.Color(255, 255, 255));
        jLabel22.setText("EMIT UF");
        getContentPane().add(jLabel22, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 0, 400, 50));

        jLabel21.setFont(new java.awt.Font("Tahoma", 0, 24)); // NOI18N
        jLabel21.setForeground(new java.awt.Color(153, 153, 153));
        jLabel21.setText("ACCEUIL");
        jLabel21.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel21MouseEntered(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel21MouseClicked(evt);
            }
        });
        jLabel21.addMouseMotionListener(new java.awt.event.MouseMotionAdapter() {
            public void mouseDragged(java.awt.event.MouseEvent evt) {
                jLabel21MouseDragged(evt);
            }
        });
        getContentPane().add(jLabel21, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 80, 410, 20));

        visiter5.setBackground(new java.awt.Color(153, 153, 153));
        visiter5.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        visiter5.setForeground(new java.awt.Color(153, 153, 153));
        visiter5.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        visiter5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG"))); // NOI18N
        visiter5.setText("  Reservation");
        visiter5.setAlignmentX(10.0F);
        visiter5.setAlignmentY(10.0F);
        visiter5.setMinimumSize(new java.awt.Dimension(1473, 500));
        getContentPane().add(visiter5, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 490, 10, 50));

        visiter4.setBackground(new java.awt.Color(153, 153, 153));
        visiter4.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        visiter4.setForeground(new java.awt.Color(153, 153, 153));
        visiter4.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        visiter4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG"))); // NOI18N
        visiter4.setText("  Reservation");
        visiter4.setAlignmentX(10.0F);
        visiter4.setAlignmentY(10.0F);
        visiter4.setMinimumSize(new java.awt.Dimension(1473, 500));
        getContentPane().add(visiter4, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 440, 10, 50));

        visiter3.setBackground(new java.awt.Color(153, 153, 153));
        visiter3.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        visiter3.setForeground(new java.awt.Color(153, 153, 153));
        visiter3.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        visiter3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG"))); // NOI18N
        visiter3.setText("  Reservation");
        visiter3.setAlignmentX(10.0F);
        visiter3.setAlignmentY(10.0F);
        visiter3.setMinimumSize(new java.awt.Dimension(1473, 500));
        getContentPane().add(visiter3, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 390, 10, 50));

        visiter2.setBackground(new java.awt.Color(153, 153, 153));
        visiter2.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        visiter2.setForeground(new java.awt.Color(153, 153, 153));
        visiter2.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        visiter2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/GRIS.PNG"))); // NOI18N
        visiter2.setText("  Reservation");
        visiter2.setAlignmentX(10.0F);
        visiter2.setAlignmentY(10.0F);
        visiter2.setMinimumSize(new java.awt.Dimension(1473, 500));
        getContentPane().add(visiter2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 340, 10, 50));

        jLabel20.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel20.setForeground(new java.awt.Color(153, 153, 153));
        jLabel20.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel20.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Books_24px.png"))); // NOI18N
        jLabel20.setText(" liste recrutés");
        jLabel20.setAlignmentX(10.0F);
        jLabel20.setAlignmentY(10.0F);
        jLabel20.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel20.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel20MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel20, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 490, 200, 50));

        jLabel19.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel19.setForeground(new java.awt.Color(153, 153, 153));
        jLabel19.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel19.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/User Group Man Man_24px.png"))); // NOI18N
        jLabel19.setText(" Candidat");
        jLabel19.setAlignmentX(10.0F);
        jLabel19.setAlignmentY(10.0F);
        jLabel19.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel19.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel19MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel19, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 440, 200, 50));

        jLabel18.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel18.setForeground(new java.awt.Color(153, 153, 153));
        jLabel18.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel18.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Untested_24px.png"))); // NOI18N
        jLabel18.setText(" Recrutement");
        jLabel18.setAlignmentX(10.0F);
        jLabel18.setAlignmentY(10.0F);
        jLabel18.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel18.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel18MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel18MouseEntered(evt);
            }
        });
        getContentPane().add(jLabel18, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 390, 200, 50));

        jLabel17.setFont(new java.awt.Font("Tahoma", 0, 18)); // NOI18N
        jLabel17.setForeground(new java.awt.Color(153, 153, 153));
        jLabel17.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel17.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/Home_24px_1.png"))); // NOI18N
        jLabel17.setText(" Acceuil");
        jLabel17.setAlignmentX(10.0F);
        jLabel17.setAlignmentY(10.0F);
        jLabel17.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel17.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel17MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel17, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 340, 200, 50));

        jLabel16.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/grandBlanc.PNG"))); // NOI18N
        getContentPane().add(jLabel16, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 284, 230, 390));

        date.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        date.setForeground(new java.awt.Color(153, 153, 153));
        getContentPane().add(date, new org.netbeans.lib.awtextra.AbsoluteConstraints(30, 230, 180, 20));

        heure.setFont(new java.awt.Font("Tahoma", 0, 70)); // NOI18N
        heure.setForeground(new java.awt.Color(180, 177, 177));
        heure.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        heure.setText("10:00");
        getContentPane().add(heure, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 140, 210, 100));

        jLabel15.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG"))); // NOI18N
        jLabel15.setText("jLabel15");
        getContentPane().add(jLabel15, new org.netbeans.lib.awtextra.AbsoluteConstraints(20, 140, 200, 130));

        jLabel13.setFont(new java.awt.Font("Tahoma", 1, 14)); // NOI18N
        jLabel13.setForeground(new java.awt.Color(255, 255, 255));
        jLabel13.setHorizontalAlignment(javax.swing.SwingConstants.RIGHT);
        getContentPane().add(jLabel13, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 10, 130, 30));

        jLabel12.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/fond.PNG"))); // NOI18N
        getContentPane().add(jLabel12, new org.netbeans.lib.awtextra.AbsoluteConstraints(1110, 10, 140, 40));

        jLabel8.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/fond.PNG"))); // NOI18N
        jLabel8.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel8.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel8MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel8, new org.netbeans.lib.awtextra.AbsoluteConstraints(1310, 10, 40, 40));

        jLabel9.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/xPNG.PNG"))); // NOI18N
        jLabel9.setText("jLabel9");
        jLabel9.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel9.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel9MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel9, new org.netbeans.lib.awtextra.AbsoluteConstraints(1300, 10, 50, 40));

        jLabel10.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/fond.PNG"))); // NOI18N
        getContentPane().add(jLabel10, new org.netbeans.lib.awtextra.AbsoluteConstraints(1300, 4, 50, 50));

        jLabel7.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/fond.PNG"))); // NOI18N
        getContentPane().add(jLabel7, new org.netbeans.lib.awtextra.AbsoluteConstraints(10, 10, 90, 40));

        jPanel2.setBackground(new java.awt.Color(33, 150, 243));
        jPanel2.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(33, 150, 243)));

        jLabel46.setFont(new java.awt.Font("Brush Script MT", 0, 18)); // NOI18N
        jLabel46.setForeground(new java.awt.Color(255, 255, 255));

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(1340, Short.MAX_VALUE)
                .addComponent(jLabel46)
                .addContainerGap())
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        getContentPane().add(jPanel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 700, 1350, 40));

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/teinte.PNG"))); // NOI18N
        jLabel4.setText("jLabel4");
        getContentPane().add(jLabel4, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 70, 150, 40));

        SousPanelPrincipal.setBackground(new java.awt.Color(255, 255, 255));
        SousPanelPrincipal.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(204, 204, 204)));

        panelPrincipal.setBackground(new java.awt.Color(255, 255, 255));
        panelPrincipal.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                panelPrincipalMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout panelPrincipalLayout = new javax.swing.GroupLayout(panelPrincipal);
        panelPrincipal.setLayout(panelPrincipalLayout);
        panelPrincipalLayout.setHorizontalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 1098, Short.MAX_VALUE)
        );
        panelPrincipalLayout.setVerticalGroup(
            panelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGap(0, 568, Short.MAX_VALUE)
        );

        javax.swing.GroupLayout SousPanelPrincipalLayout = new javax.swing.GroupLayout(SousPanelPrincipal);
        SousPanelPrincipal.setLayout(SousPanelPrincipalLayout);
        SousPanelPrincipalLayout.setHorizontalGroup(
            SousPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrincipal, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );
        SousPanelPrincipalLayout.setVerticalGroup(
            SousPanelPrincipalLayout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(panelPrincipal, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        getContentPane().add(SousPanelPrincipal, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 120, 1100, 570));

        jLabel3.setBackground(new java.awt.Color(33, 150, 243));
        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/fond.PNG"))); // NOI18N
        jLabel3.setText("jLabel3");
        getContentPane().add(jLabel3, new org.netbeans.lib.awtextra.AbsoluteConstraints(240, 10, 210, 40));

        jLabel25.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/gr.PNG"))); // NOI18N
        jLabel25.setText("jLabel25");
        getContentPane().add(jLabel25, new org.netbeans.lib.awtextra.AbsoluteConstraints(610, 680, 730, 30));

        jLabel24.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/gr.PNG"))); // NOI18N
        jLabel24.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 1, 0, 0, new java.awt.Color(255, 255, 255)));
        getContentPane().add(jLabel24, new org.netbeans.lib.awtextra.AbsoluteConstraints(230, 680, 1060, 30));

        jLabel6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blancb.PNG"))); // NOI18N
        jLabel6.setText("jLabel6");
        getContentPane().add(jLabel6, new org.netbeans.lib.awtextra.AbsoluteConstraints(-6, 680, 240, 50));

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/fond.PNG"))); // NOI18N
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });
        getContentPane().add(jLabel2, new org.netbeans.lib.awtextra.AbsoluteConstraints(96, 10, 130, 40));

        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/arrierePlan.PNG"))); // NOI18N
        jLabel1.setVerticalAlignment(javax.swing.SwingConstants.TOP);
        jLabel1.setBorder(javax.swing.BorderFactory.createMatteBorder(1, 1, 1, 1, new java.awt.Color(255, 255, 255)));
        jLabel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel1MouseEntered(evt);
            }
        });
        getContentPane().add(jLabel1, new org.netbeans.lib.awtextra.AbsoluteConstraints(0, 0, 1350, 730));

        form2.setBackground(new java.awt.Color(255, 255, 255));
        form2.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        form2.setForeground(new java.awt.Color(33, 150, 243));
        form2.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        form2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG"))); // NOI18N
        form2.setText("Formulaire");
        form2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        form2.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        form2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                form2MouseEntered(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                form2MouseClicked(evt);
            }
        });
        getContentPane().add(form2, new org.netbeans.lib.awtextra.AbsoluteConstraints(250, 120, 140, 30));

        messageDec.setText("A");
        getContentPane().add(messageDec, new org.netbeans.lib.awtextra.AbsoluteConstraints(960, 20, -1, 20));

        setSize(new java.awt.Dimension(1348, 741));
        setLocationRelativeTo(null);
    }// </editor-fold>//GEN-END:initComponents

    private void jLabel9MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel9MouseClicked
        System.exit(0);
    }//GEN-LAST:event_jLabel9MouseClicked

    private void jLabel8MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel8MouseClicked
        System.exit(0);
    }//GEN-LAST:event_jLabel8MouseClicked

    private void jLabel17MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel17MouseClicked
        visiter2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/GRIS.PNG")));
        jLabel21.setText("ACCEUIL");
        visiter3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        Graphe();
     
        
    }//GEN-LAST:event_jLabel17MouseClicked

    private void formWindowOpened(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowOpened
      
    }//GEN-LAST:event_formWindowOpened

    private void jLabel18MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MouseClicked
        visiter3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/GRIS.PNG")));
        jLabel21.setText("RECRUTEMENT");
        visiter2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        this.panelPrincipal.removeAll();
        this.panelPrincipal.repaint();     
        RecrutementListe r = new RecrutementListe();
        this.panelPrincipal.add(r);
        try {
            r.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Acceuil.class.getName()).log(Level.SEVERE, null, ex);
        }
        r.show();  
       
    }//GEN-LAST:event_jLabel18MouseClicked

    private void jLabel19MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel19MouseClicked
        visiter4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/GRIS.PNG")));
        jLabel21.setText("CANDIDAT");
        visiter3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
         this.panelPrincipal.removeAll();
        this.panelPrincipal.repaint();     
        CandidatListe r = new CandidatListe();
        this.panelPrincipal.add(r);
        try {
            r.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Acceuil.class.getName()).log(Level.SEVERE, null, ex);
        }
        r.show();
     
    }//GEN-LAST:event_jLabel19MouseClicked

    private void jLabel20MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel20MouseClicked
      visiter5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/GRIS.PNG")));
        jLabel21.setText("LES RECRUTES");
        visiter3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
         this.panelPrincipal.removeAll();
        this.panelPrincipal.repaint();     
        LesRecrutes r = new LesRecrutes();
        this.panelPrincipal.add(r);
        try {
            r.setMaximum(true);
        } catch (PropertyVetoException ex) {
            Logger.getLogger(Acceuil.class.getName()).log(Level.SEVERE, null, ex);
        }
        r.show();
         
        
        
    }//GEN-LAST:event_jLabel20MouseClicked

    private void jLabel21MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseClicked

    }//GEN-LAST:event_jLabel21MouseClicked

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
       
    }//GEN-LAST:event_jLabel2MouseClicked

    private void jLabel23MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel23MouseClicked
        visiter6.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/GRIS.PNG")));
        jLabel21.setText("STOCK");
        visiter2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
        visiter5.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/blanc.PNG")));
       
       
    }//GEN-LAST:event_jLabel23MouseClicked

    private void jLabel18MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel18MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel18MouseEntered

    private void form2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_form2MouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_form2MouseEntered

    private void form2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_form2MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_form2MouseClicked

    private void jLabel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel1MouseEntered
       
            
    }//GEN-LAST:event_jLabel1MouseEntered

    private void formWindowActivated(java.awt.event.WindowEvent evt) {//GEN-FIRST:event_formWindowActivated
        
    }//GEN-LAST:event_formWindowActivated

    private void formMouseEntered1(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseEntered1
        // TODO add your handling code here:
      
    }//GEN-LAST:event_formMouseEntered1

    private void jLabel21MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseEntered
        // TODO add your handling code here:
        
     
    }//GEN-LAST:event_jLabel21MouseEntered

    private void jLabel21MouseDragged(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel21MouseDragged
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel21MouseDragged

    private void panelPrincipalMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_panelPrincipalMouseEntered
        
    }//GEN-LAST:event_panelPrincipalMouseEntered

    private void jLabel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseClicked
        if(messageDec.getText()=="A"){visibleDeconnecte();messageDec.setText("B");
                    
        }else if(messageDec.getText()=="B"){invisibleDeconnecte(); messageDec.setText("A") ; }
        
    }//GEN-LAST:event_jLabel5MouseClicked

    private void deconnecterMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_deconnecterMouseClicked
        System.exit(0);
    }//GEN-LAST:event_deconnecterMouseClicked

    private void chagerCompteMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_chagerCompteMouseClicked
        this.setVisible(false);
        Login lo = new Login();
        lo.setVisible(true);
        
    }//GEN-LAST:event_chagerCompteMouseClicked

    private void creerMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_creerMouseClicked
        User u = new User();
        u.setVisible(true);
    }//GEN-LAST:event_creerMouseClicked

    /**
     * @param args formRecmand line arguments
     */
    public static void main(String args[]) {
        /* Set the Nimbus look and feel */
        //<editor-fold defaultstate="collapsed" desc=" Look and feel setting code (optional) ">
        /* If Nimbus (introduced in Java SE 6) is not available, stay with the default look aformRec.
         * For details see http://download.oracle.com/javase/tutorial/uiswing/lookandfeel/plaf.html 
         */
        try {
            for (javax.swing.UIManager.LookAndFeelInfo info : javax.swing.UIManager.getInstalledLookAndFeels()) {
                if ("Nimbus".equals(info.getName())) {
                    javax.swing.UIManager.setLookAndFeel(info.getClassName());
                    break;
                }
            }
        } catch (ClassNotFoundException ex) {
            java.util.logging.Logger.getLogger(Acceuil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (InstantiationException ex) {
            java.util.logging.Logger.getLogger(Acceuil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (IllegalAccessException ex) {
            java.util.logging.Logger.getLogger(Acceuil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        } catch (javax.swing.UnsupportedLookAndFeelException ex) {
            java.util.logging.Logger.getLogger(Acceuil.class.getName()).log(java.util.logging.Level.SEVERE, null, ex);
        }
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>
        //</editor-fold>

        /* Create and display the form */
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Acceuil().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JPanel SousPanelPrincipal;
    private javax.swing.JLabel chagerCompte;
    private javax.swing.JLabel creer;
    private javax.swing.JLabel date;
    private javax.swing.JLabel deconnecter;
    private javax.swing.JLabel form2;
    private javax.swing.JLabel heure;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel10;
    private javax.swing.JLabel jLabel12;
    private javax.swing.JLabel jLabel13;
    private javax.swing.JLabel jLabel15;
    private javax.swing.JLabel jLabel16;
    private javax.swing.JLabel jLabel17;
    private javax.swing.JLabel jLabel18;
    private javax.swing.JLabel jLabel19;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel20;
    private javax.swing.JLabel jLabel21;
    private javax.swing.JLabel jLabel22;
    private javax.swing.JLabel jLabel23;
    private javax.swing.JLabel jLabel24;
    private javax.swing.JLabel jLabel25;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JLabel labelDeconnect;
    private javax.swing.JLabel messageDec;
    private javax.swing.JPanel panelPrincipal;
    private javax.swing.JLabel user;
    private javax.swing.JLabel visiter2;
    private javax.swing.JLabel visiter3;
    private javax.swing.JLabel visiter4;
    private javax.swing.JLabel visiter5;
    private javax.swing.JLabel visiter6;
    // End of variables declaration//GEN-END:variables
}
