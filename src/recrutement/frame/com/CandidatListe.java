/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package recrutement.frame.com;

import java.text.MessageFormat;
import javax.swing.ImageIcon;
import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import recrutement.gestion.com.gestionCandidat;
import recrutement.gestion.com.gestionProfil;
import recrutement.gestion.com.gestionRecru;
import recrutement.gestion.com.gestionRecrutement;

/**
 *
 * @author Tsiry
 */
public class CandidatListe extends javax.swing.JInternalFrame {

    public gestionCandidat gc = new gestionCandidat();
    public gestionProfil gp = new gestionProfil();
    public ImageIcon Format = null;
    public String txt ;
    public CandidatListe() {
        initComponents();
        jScrollPane3.setBorder(null);
        cacherbarMenu();
        combo();
        idRec();
        candidatAuRecrutement();
        selectionnerPremiereLigne();
        compte();
    }
   
     public void cacherbarMenu(){
        
        this.setBorder(javax.swing.BorderFactory.createEmptyBorder(0, 0, 0, 0));
        BasicInternalFrameUI bui = (BasicInternalFrameUI)this.getUI();
        bui.setNorthPane(null);
        
    }
     
    public void compte(){
        gestionCandidat gc = new gestionCandidat();
        gc.compte(idRecrutement.getText(), effectif);
    }  
    public void combo(){
        gestionCandidat gc = new gestionCandidat();
        gc.combo(combo);
    }
    
     public void idRec(){
        gestionCandidat gc = new gestionCandidat();
        gc.idRec(combo,idRecrutement);
     }
     
     public void candidatAuRecrutement(){
        gestionCandidat gc = new gestionCandidat();
        gc.listeCandidat(jTable1, idRecrutement);
     }
     public void vider(){
        id.setText("");nom.setText("");prenom.setText(""); dateNaiss.setText(""); sexe.setText(""); age.setText(""); age.setText(""); cin.setText(""); adrs.setText(""); tel.setText("");
        profil.setText(""); mail.setText("");etat.setText("");
        exp.setText("0"); niv.setText("0"); txtSpeci.setText("0");txtFrns.setText("0"); txtAngls.setText("0") ; txtDiplo.setText("0"); performance.setText("20%");
        image.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/default.PNG")));
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/perfo20.PNG"))); 
     }
     
     public void selectionnerPremiereLigne(){
        int i = jTable1.getRowCount();
        if(i > 0){
            jTable1.setRowSelectionInterval(0, 0);
            gc.image(String.valueOf(jTable1.getValueAt(0,0)) , image, Format);
            gc.deplacement(String.valueOf(jTable1.getValueAt(0,0)), combo  ,id ,  nom , prenom , dateNaiss , sexe , age , cin , adrs , tel,mail , etat);
            gp.deplacement(String.valueOf(jTable1.getValueAt(0,0)), profil);
            gp.affichagePerfo(String.valueOf(jTable1.getValueAt(0,0)), exp, niv, txtSpeci, txtFrns, txtAngls, txtDiplo, performance);
            imagePerfo();
        }
    }
    public void ligneSelect(){
        int j = jTable1.getSelectedRow();
        ligneSelectionner.setText(String.valueOf(j));
    }
    
    public void selectionnerApresModif(){
     jTable1.setRowSelectionInterval(Integer.parseInt(ligneSelectionner.getText()),Integer.parseInt(ligneSelectionner.getText()));
        
    }
    
    public void imprimer(){
        MessageFormat header =new MessageFormat("Candidats au "+combo.getSelectedItem().toString()+"");
        MessageFormat footer =new MessageFormat("Page{0,number,integer}");
        try{
            jTable1.print(JTable.PrintMode.FIT_WIDTH, header, footer);
        }catch(java.awt.print.PrinterException e){System.err.format("erreur d'impression",e.getMessage());}
    }
    
   
    public void actualiserModification(){
        if("B".equals(commentaireModif.getText())){
            gc.idRecApresModification(id.getText(),idRecApresModification);
            gestionCandidat gc = new gestionCandidat();
            gc.actualiser(jTable1 , idRecrutement);compte();
            if(!"".equals(idRecrutement.getText()) && !"".equals(idRecApresModification.getText()) && idRecrutement.getText().equals(idRecApresModification.getText())){
                selectionnerApresModif();
                gc.image( id.getText(), image , Format );
                gc.afficherModification(nom, prenom ,  dateNaiss ,  sexe ,   age ,   cin ,   adrs , tel , id,mail , etat);
                gp.afficherModification(id.getText(), profil);
                gp.affichagePerfo(id.getText(), exp, niv, txtSpeci, txtFrns, txtAngls, txtDiplo, performance);
                imagePerfo();
                selectionnerApresModif();
            }
            if(!"".equals(idRecrutement.getText()) && !"".equals(idRecApresModification.getText()) && !idRecrutement.getText().equals(idRecApresModification.getText()) && jTable1.getRowCount() > 0){
                selectionnerPremiereLigne();
            }if(!"".equals(idRecrutement.getText()) && !"".equals(idRecApresModification.getText()) && !idRecrutement.getText().equals(idRecApresModification.getText()) && jTable1.getRowCount() <= 0){
                vider();
            }
            
            commentaireModif.setText("A");
            
        }
    }
    public void actualiserAjout(){
        if("B".equals(commentaireAjout.getText())){
            gc.idRecApresModification(id.getText(),idRecApresModification);
            gestionCandidat gc = new gestionCandidat();
            gc.actualiser(jTable1 , idRecrutement);
        
                gc.image( id.getText(), image , Format );
                gc.afficherModification(nom, prenom ,  dateNaiss ,  sexe ,   age ,   cin ,   adrs , tel , id,mail , etat);
                gp.afficherModification(id.getText(), profil);
                selectionnerApresModif();
            
            commentaireAjout.setText("A");
        }
    }
    
    public void actualiserApresRecruter(){
    
     if("B".equals(commentaireRecru.getText())){
         gestionCandidat gc = new gestionCandidat();
         gc.actualiser(jTable1 , idRecrutement);compte();
         gc.afficherModification(nom, prenom ,  dateNaiss ,  sexe ,   age ,   cin ,   adrs , tel , id,mail , etat);
         jTable1.setRowSelectionInterval(Integer.parseInt(ligneSelectionner.getText()),Integer.parseInt(ligneSelectionner.getText()));
         commentaireRecru.setText("A");
     }
    }
    
    public void imagePerfo(){
        if("10%".equals(performance.getText())){
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/perfo20.PNG"))); 
        }
        if("20%".equals(performance.getText())){
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/perfo20.PNG"))); 
        }
        if("30%".equals(performance.getText())){
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/perfo30.PNG"))); 
        } 
        if("40%".equals(performance.getText())){
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/perfo40.PNG"))); 
        }
        if("50%".equals(performance.getText())){
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/perfo50.PNG"))); 
        }
        if("60%".equals(performance.getText())){
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/perfo60.PNG"))); 
        }
        if("70%".equals(performance.getText())){
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/perfo70.PNG"))); 
        }
        if("80%".equals(performance.getText())){
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/perfo80.PNG"))); 
        }
        if("90%".equals(performance.getText())){
            jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/perfo90.PNG"))); 
        }
        
        
    
    }
    
    
    
    
   
    
   
     
   
    /**
     * This method is called from within the constructor to initialize the form.
     * WARNING: Do NOT modify this code. The content of this method is always
     * regenerated by the Form Editor.
     */
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jPanel3 = new javax.swing.JPanel();
        jPanel4 = new javax.swing.JPanel();
        idRec = new javax.swing.JLabel();
        jScrollPane3 = new javax.swing.JScrollPane();
        profil = new javax.swing.JTextArea();
        jPanel5 = new javax.swing.JPanel();
        jPanel6 = new javax.swing.JPanel();
        jScrollPane1 = new javax.swing.JScrollPane();
        jTable1 = new javax.swing.JTable();
        jLabel28 = new javax.swing.JLabel();
        jLabel29 = new javax.swing.JLabel();
        recherche = new javax.swing.JTextField();
        jLabel30 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        effectif = new javax.swing.JLabel();
        combo = new javax.swing.JComboBox<>();
        jPanel1 = new javax.swing.JPanel();
        jDesktopPane1 = new javax.swing.JDesktopPane();
        jScrollPane2 = new javax.swing.JScrollPane();
        image = new javax.swing.JLabel();
        jLabel32 = new javax.swing.JLabel();
        jLabel33 = new javax.swing.JLabel();
        jLabel34 = new javax.swing.JLabel();
        jLabel36 = new javax.swing.JLabel();
        jLabel38 = new javax.swing.JLabel();
        jLabel40 = new javax.swing.JLabel();
        jLabel42 = new javax.swing.JLabel();
        jLabel44 = new javax.swing.JLabel();
        id = new javax.swing.JLabel();
        nom = new javax.swing.JLabel();
        prenom = new javax.swing.JLabel();
        dateNaiss = new javax.swing.JLabel();
        sexe = new javax.swing.JLabel();
        age = new javax.swing.JLabel();
        cin = new javax.swing.JLabel();
        adrs = new javax.swing.JLabel();
        tel = new javax.swing.JLabel();
        idRecApresModification = new javax.swing.JLabel();
        jLabel46 = new javax.swing.JLabel();
        jLabel47 = new javax.swing.JLabel();
        mail = new javax.swing.JLabel();
        etat = new javax.swing.JLabel();
        jPanel2 = new javax.swing.JPanel();
        jLabel2 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        btnRec = new javax.swing.JLabel();
        idRecrutement = new javax.swing.JLabel();
        commentaireAjout = new javax.swing.JLabel();
        ligneSelectionner = new javax.swing.JLabel();
        commentaireRecru = new javax.swing.JLabel();
        commentaireModif = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        performance = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        exp = new javax.swing.JLabel();
        niv = new javax.swing.JLabel();
        txtSpeci = new javax.swing.JLabel();
        txtFrns = new javax.swing.JLabel();
        txtAngls = new javax.swing.JLabel();
        txtDiplo = new javax.swing.JLabel();

        addInternalFrameListener(new javax.swing.event.InternalFrameListener() {
            public void internalFrameClosed(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameOpened(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameOpened(evt);
            }
            public void internalFrameIconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeactivated(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameDeiconified(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameClosing(javax.swing.event.InternalFrameEvent evt) {
            }
            public void internalFrameActivated(javax.swing.event.InternalFrameEvent evt) {
                formInternalFrameActivated(evt);
            }
        });
        addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                formMouseEntered(evt);
            }
        });

        jPanel3.setBackground(new java.awt.Color(255, 255, 255));
        jPanel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel3MouseEntered(evt);
            }
        });

        jPanel4.setBackground(new java.awt.Color(255, 255, 255));
        jPanel4.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 1, 1, new java.awt.Color(204, 204, 204)), "Profil et performance ", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        jPanel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel4MouseEntered(evt);
            }
        });

        idRec.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        idRec.setForeground(new java.awt.Color(102, 102, 102));
        idRec.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        idRec.setText(" ");
        idRec.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                idRecMouseClicked(evt);
            }
        });

        profil.setEditable(false);
        profil.setColumns(20);
        profil.setForeground(new java.awt.Color(102, 102, 102));
        profil.setRows(5);
        profil.setBorder(null);
        profil.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                profilMouseEntered(evt);
            }
        });
        jScrollPane3.setViewportView(profil);

        javax.swing.GroupLayout jPanel4Layout = new javax.swing.GroupLayout(jPanel4);
        jPanel4.setLayout(jPanel4Layout);
        jPanel4Layout.setHorizontalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addComponent(jScrollPane3, javax.swing.GroupLayout.PREFERRED_SIZE, 503, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(idRec, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );
        jPanel4Layout.setVerticalGroup(
            jPanel4Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel4Layout.createSequentialGroup()
                .addComponent(idRec)
                .addContainerGap(450, Short.MAX_VALUE))
            .addComponent(jScrollPane3)
        );

        jPanel5.setBackground(new java.awt.Color(255, 255, 255));
        jPanel5.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 0, 1, new java.awt.Color(204, 204, 204)), "Type de recrutement", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        jPanel5.setForeground(new java.awt.Color(153, 153, 153));
        jPanel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jPanel5MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel5MouseEntered(evt);
            }
        });

        jPanel6.setBackground(new java.awt.Color(255, 255, 255));
        jPanel6.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 0, 0, new java.awt.Color(204, 204, 204)), "Candidats participants", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        jPanel6.setForeground(new java.awt.Color(153, 153, 153));
        jPanel6.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel6MouseEntered(evt);
            }
        });

        jScrollPane1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jScrollPane1MouseEntered(evt);
            }
        });

        jTable1.setModel(new javax.swing.table.DefaultTableModel(
            new Object [][] {
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null},
                {null, null, null}
            },
            new String [] {
                "Title 1", "Title 2", "Title 3"
            }
        ));
        jTable1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jTable1MouseClicked(evt);
            }
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jTable1MouseEntered(evt);
            }
        });
        jTable1.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyReleased(java.awt.event.KeyEvent evt) {
                jTable1KeyReleased(evt);
            }
            public void keyPressed(java.awt.event.KeyEvent evt) {
                jTable1KeyPressed(evt);
            }
        });
        jScrollPane1.setViewportView(jTable1);

        jLabel28.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel28.setForeground(new java.awt.Color(102, 102, 102));
        jLabel28.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel28.setText("Recherche :");
        jLabel28.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel28MouseClicked(evt);
            }
        });

        jLabel29.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel29.setForeground(new java.awt.Color(153, 153, 153));
        jLabel29.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel29.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/textfield_1.PNG"))); // NOI18N
        jLabel29.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel29MouseClicked(evt);
            }
        });

        recherche.setFont(new java.awt.Font("Tahoma", 0, 13)); // NOI18N
        recherche.setBorder(null);
        recherche.addKeyListener(new java.awt.event.KeyAdapter() {
            public void keyPressed(java.awt.event.KeyEvent evt) {
                rechercheKeyPressed(evt);
            }
            public void keyReleased(java.awt.event.KeyEvent evt) {
                rechercheKeyReleased(evt);
            }
            public void keyTyped(java.awt.event.KeyEvent evt) {
                rechercheKeyTyped(evt);
            }
        });

        jLabel30.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel30.setForeground(new java.awt.Color(102, 102, 102));
        jLabel30.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel30.setText("Imprimer une liste");
        jLabel30.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255, 255, 255)));
        jLabel30.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel30.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel30MouseEntered(evt);
            }
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel30MouseClicked(evt);
            }
            public void mouseExited(java.awt.event.MouseEvent evt) {
                jLabel30MouseExited(evt);
            }
        });

        jLabel5.setForeground(new java.awt.Color(102, 102, 102));
        jLabel5.setText("Nombre de participant:");
        jLabel5.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jLabel5MouseEntered(evt);
            }
        });

        effectif.setForeground(new java.awt.Color(102, 102, 102));
        effectif.setText("00");
        effectif.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                effectifMouseEntered(evt);
            }
        });

        javax.swing.GroupLayout jPanel6Layout = new javax.swing.GroupLayout(jPanel6);
        jPanel6.setLayout(jPanel6Layout);
        jPanel6Layout.setHorizontalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jScrollPane1, javax.swing.GroupLayout.DEFAULT_SIZE, 459, Short.MAX_VALUE)
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addComponent(jLabel30)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel28, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(recherche, javax.swing.GroupLayout.DEFAULT_SIZE, 158, Short.MAX_VALUE)))
                    .addGroup(jPanel6Layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addComponent(jLabel5)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(effectif, javax.swing.GroupLayout.PREFERRED_SIZE, 20, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );
        jPanel6Layout.setVerticalGroup(
            jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel6Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel28)
                    .addComponent(recherche, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel30))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel29, javax.swing.GroupLayout.PREFERRED_SIZE, 1, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jScrollPane1, javax.swing.GroupLayout.PREFERRED_SIZE, 150, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addGroup(jPanel6Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(effectif, javax.swing.GroupLayout.PREFERRED_SIZE, 14, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(jLabel5))
                .addGap(178, 178, 178))
        );

        combo.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        combo.setForeground(new java.awt.Color(102, 102, 102));
        combo.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                comboItemStateChanged(evt);
            }
        });

        jPanel1.setBackground(new java.awt.Color(255, 255, 255));
        jPanel1.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(1, 0, 0, 0, new java.awt.Color(204, 204, 204)), "Information personnelle", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 12))); // NOI18N
        jPanel1.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel1MouseEntered(evt);
            }
        });

        jScrollPane2.setBackground(new java.awt.Color(255, 255, 255));

        image.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        image.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/default.PNG"))); // NOI18N
        image.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);
        jScrollPane2.setViewportView(image);

        jDesktopPane1.setLayer(jScrollPane2, javax.swing.JLayeredPane.DEFAULT_LAYER);

        javax.swing.GroupLayout jDesktopPane1Layout = new javax.swing.GroupLayout(jDesktopPane1);
        jDesktopPane1.setLayout(jDesktopPane1Layout);
        jDesktopPane1Layout.setHorizontalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 174, Short.MAX_VALUE)
        );
        jDesktopPane1Layout.setVerticalGroup(
            jDesktopPane1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jScrollPane2, javax.swing.GroupLayout.DEFAULT_SIZE, 179, Short.MAX_VALUE)
        );

        jLabel32.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel32.setForeground(new java.awt.Color(102, 102, 102));
        jLabel32.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel32.setText("Numero");
        jLabel32.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel32MouseClicked(evt);
            }
        });

        jLabel33.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel33.setForeground(new java.awt.Color(102, 102, 102));
        jLabel33.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel33.setText("Nom :");
        jLabel33.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel33MouseClicked(evt);
            }
        });

        jLabel34.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel34.setForeground(new java.awt.Color(102, 102, 102));
        jLabel34.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel34.setText("Prenom :");
        jLabel34.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel34MouseClicked(evt);
            }
        });

        jLabel36.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel36.setForeground(new java.awt.Color(102, 102, 102));
        jLabel36.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel36.setText("Nait le :");
        jLabel36.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel36MouseClicked(evt);
            }
        });

        jLabel38.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel38.setForeground(new java.awt.Color(102, 102, 102));
        jLabel38.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel38.setText("Sexe :");
        jLabel38.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel38MouseClicked(evt);
            }
        });

        jLabel40.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel40.setForeground(new java.awt.Color(102, 102, 102));
        jLabel40.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel40.setText("age :");
        jLabel40.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel40MouseClicked(evt);
            }
        });

        jLabel42.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel42.setForeground(new java.awt.Color(102, 102, 102));
        jLabel42.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel42.setText("CIN :");
        jLabel42.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel42MouseClicked(evt);
            }
        });

        jLabel44.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel44.setForeground(new java.awt.Color(102, 102, 102));
        jLabel44.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel44.setText("Adresse :");
        jLabel44.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel44MouseClicked(evt);
            }
        });

        id.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        id.setForeground(new java.awt.Color(102, 102, 102));
        id.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        id.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                idMouseClicked(evt);
            }
        });

        nom.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        nom.setForeground(new java.awt.Color(102, 102, 102));
        nom.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        nom.setText(" ");
        nom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                nomMouseClicked(evt);
            }
        });

        prenom.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        prenom.setForeground(new java.awt.Color(102, 102, 102));
        prenom.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        prenom.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                prenomMouseClicked(evt);
            }
        });

        dateNaiss.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        dateNaiss.setForeground(new java.awt.Color(102, 102, 102));
        dateNaiss.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        dateNaiss.setText(" ");
        dateNaiss.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                dateNaissMouseClicked(evt);
            }
        });

        sexe.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        sexe.setForeground(new java.awt.Color(102, 102, 102));
        sexe.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        sexe.setText(" ");
        sexe.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                sexeMouseClicked(evt);
            }
        });

        age.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        age.setForeground(new java.awt.Color(102, 102, 102));
        age.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        age.setText(" ");
        age.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                ageMouseClicked(evt);
            }
        });

        cin.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        cin.setForeground(new java.awt.Color(102, 102, 102));
        cin.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        cin.setText(" ");
        cin.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                cinMouseClicked(evt);
            }
        });

        adrs.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        adrs.setForeground(new java.awt.Color(102, 102, 102));
        adrs.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        adrs.setText(" ");
        adrs.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                adrsMouseClicked(evt);
            }
        });

        tel.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        tel.setForeground(new java.awt.Color(102, 102, 102));
        tel.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        tel.setText(" ");
        tel.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                telMouseClicked(evt);
            }
        });

        idRecApresModification.setForeground(new java.awt.Color(255, 255, 255));

        jLabel46.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel46.setForeground(new java.awt.Color(102, 102, 102));
        jLabel46.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel46.setText("Tel");
        jLabel46.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel46MouseClicked(evt);
            }
        });

        jLabel47.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel47.setForeground(new java.awt.Color(102, 102, 102));
        jLabel47.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        jLabel47.setText("Email :");
        jLabel47.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel47MouseClicked(evt);
            }
        });

        mail.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        mail.setForeground(new java.awt.Color(102, 102, 102));
        mail.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        mail.setText(" ");
        mail.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mailMouseClicked(evt);
            }
        });

        etat.setForeground(new java.awt.Color(255, 255, 255));
        etat.setText(" ");

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel46, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(tel, javax.swing.GroupLayout.PREFERRED_SIZE, 145, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap())
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addComponent(jLabel47, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(18, 18, 18)
                                .addComponent(mail, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING, false)
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel44, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(adrs, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel42, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(cin, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel40, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(age, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel33, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(nom, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addComponent(jLabel38, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(sexe, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                                    .addGroup(javax.swing.GroupLayout.Alignment.LEADING, jPanel1Layout.createSequentialGroup()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                            .addComponent(jLabel34, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                                            .addComponent(jLabel36, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE))
                                        .addGap(18, 18, 18)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                                            .addComponent(prenom, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                            .addComponent(dateNaiss, javax.swing.GroupLayout.DEFAULT_SIZE, 165, Short.MAX_VALUE))))
                                .addGap(0, 0, Short.MAX_VALUE)))
                        .addGap(20, 20, 20))
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addComponent(jLabel32, javax.swing.GroupLayout.PREFERRED_SIZE, 67, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(id, javax.swing.GroupLayout.PREFERRED_SIZE, 57, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                                .addComponent(idRecApresModification)
                                .addGap(0, 0, Short.MAX_VALUE))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                .addComponent(etat, javax.swing.GroupLayout.PREFERRED_SIZE, 65, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(30, 30, 30))))))
        );
        jPanel1Layout.setVerticalGroup(
            jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel1Layout.createSequentialGroup()
                .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel32)
                            .addComponent(id, javax.swing.GroupLayout.PREFERRED_SIZE, 16, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(idRecApresModification))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel33)
                            .addComponent(nom))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jLabel34)
                            .addComponent(prenom, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel36)
                            .addComponent(dateNaiss, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel38)
                            .addComponent(sexe))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel40)
                            .addComponent(age))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel42)
                            .addComponent(cin, javax.swing.GroupLayout.PREFERRED_SIZE, 15, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel44)
                            .addComponent(adrs))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel46)
                            .addComponent(tel))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                            .addComponent(jLabel47)
                            .addComponent(mail)))
                    .addComponent(jDesktopPane1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(etat))
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
        );

        javax.swing.GroupLayout jPanel5Layout = new javax.swing.GroupLayout(jPanel5);
        jPanel5.setLayout(jPanel5Layout);
        jPanel5Layout.setHorizontalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, 463, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
            .addComponent(jPanel6, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap()
                .addComponent(combo, 0, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addContainerGap())
        );
        jPanel5Layout.setVerticalGroup(
            jPanel5Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel5Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(combo, javax.swing.GroupLayout.PREFERRED_SIZE, 41, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jPanel6, javax.swing.GroupLayout.PREFERRED_SIZE, 232, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(29, 29, 29))
        );

        jPanel2.setBackground(new java.awt.Color(255, 255, 255));
        jPanel2.setBorder(javax.swing.BorderFactory.createTitledBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 0, 0, new java.awt.Color(204, 204, 204)), "", javax.swing.border.TitledBorder.DEFAULT_JUSTIFICATION, javax.swing.border.TitledBorder.DEFAULT_POSITION, new java.awt.Font("Tahoma", 0, 14))); // NOI18N
        jPanel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseEntered(java.awt.event.MouseEvent evt) {
                jPanel2MouseEntered(evt);
            }
        });

        jLabel2.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/btnEdit.PNG"))); // NOI18N
        jLabel2.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel2.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel2MouseClicked(evt);
            }
        });

        jLabel3.setIcon(new javax.swing.ImageIcon(getClass().getResource("/image/btnSuppr.PNG"))); // NOI18N
        jLabel3.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel3.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel3MouseClicked(evt);
            }
        });

        jLabel4.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/nvCand.PNG"))); // NOI18N
        jLabel4.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        jLabel4.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                jLabel4MouseClicked(evt);
            }
        });

        btnRec.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/btnRecrut.PNG"))); // NOI18N
        btnRec.setCursor(new java.awt.Cursor(java.awt.Cursor.HAND_CURSOR));
        btnRec.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                btnRecMouseClicked(evt);
            }
        });

        javax.swing.GroupLayout jPanel2Layout = new javax.swing.GroupLayout(jPanel2);
        jPanel2.setLayout(jPanel2Layout);
        jPanel2Layout.setHorizontalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel2Layout.createSequentialGroup()
                .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                .addComponent(jLabel4)
                .addGap(2, 2, 2)
                .addComponent(jLabel2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel3)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(btnRec)
                .addGap(247, 247, 247))
        );
        jPanel2Layout.setVerticalGroup(
            jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel2Layout.createSequentialGroup()
                .addGap(1, 1, 1)
                .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel4, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGroup(jPanel2Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                            .addComponent(btnRec, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE)
                            .addComponent(jLabel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))))
        );

        idRecrutement.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        idRecrutement.setForeground(new java.awt.Color(255, 255, 255));
        idRecrutement.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        idRecrutement.setText("A");
        idRecrutement.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                idRecrutementMouseClicked(evt);
            }
        });

        commentaireAjout.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        commentaireAjout.setForeground(new java.awt.Color(255, 255, 255));
        commentaireAjout.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        commentaireAjout.setText("A");
        commentaireAjout.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                commentaireAjoutMouseClicked(evt);
            }
        });

        ligneSelectionner.setForeground(new java.awt.Color(255, 255, 255));
        ligneSelectionner.setText("0");

        commentaireRecru.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        commentaireRecru.setForeground(new java.awt.Color(255, 255, 255));
        commentaireRecru.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        commentaireRecru.setText("A");
        commentaireRecru.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                commentaireRecruMouseClicked(evt);
            }
        });

        commentaireModif.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        commentaireModif.setForeground(new java.awt.Color(255, 255, 255));
        commentaireModif.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        commentaireModif.setText("A");
        commentaireModif.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                commentaireModifMouseClicked(evt);
            }
        });

        jLabel1.setHorizontalAlignment(javax.swing.SwingConstants.CENTER);
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/perfo30.PNG"))); // NOI18N

        performance.setForeground(new java.awt.Color(255, 255, 255));
        performance.setHorizontalAlignment(javax.swing.SwingConstants.LEFT);
        performance.setIcon(new javax.swing.ImageIcon(getClass().getResource("/recrutement/icon/com/captR.PNG"))); // NOI18N
        performance.setText("20%");
        performance.setHorizontalTextPosition(javax.swing.SwingConstants.CENTER);

        jLabel6.setFont(new java.awt.Font("Tahoma", 0, 10)); // NOI18N
        jLabel6.setText("Performance");

        exp.setBackground(new java.awt.Color(255, 255, 255));
        exp.setForeground(new java.awt.Color(255, 255, 255));
        exp.setText("0");

        niv.setBackground(new java.awt.Color(255, 255, 255));
        niv.setForeground(new java.awt.Color(255, 255, 255));
        niv.setText("0");

        txtSpeci.setForeground(new java.awt.Color(255, 255, 255));
        txtSpeci.setText("0");

        txtFrns.setForeground(new java.awt.Color(255, 255, 255));
        txtFrns.setText("0");

        txtAngls.setForeground(new java.awt.Color(255, 255, 255));
        txtAngls.setText("0");

        txtDiplo.setForeground(new java.awt.Color(255, 255, 255));
        txtDiplo.setText("0");

        javax.swing.GroupLayout jPanel3Layout = new javax.swing.GroupLayout(jPanel3);
        jPanel3.setLayout(jPanel3Layout);
        jPanel3Layout.setHorizontalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addGap(0, 0, 0)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(exp)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(niv)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtSpeci)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtFrns)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(txtAngls)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(txtDiplo)
                        .addGap(127, 127, 127)
                        .addComponent(idRecrutement, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(ligneSelectionner)
                        .addGap(18, 18, 18)
                        .addComponent(commentaireRecru, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(commentaireModif, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(commentaireAjout, javax.swing.GroupLayout.PREFERRED_SIZE, 21, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, 556, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addGap(4, 4, 4)
                                .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, 520, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                    .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 77, javax.swing.GroupLayout.PREFERRED_SIZE)
                                    .addComponent(performance)
                                    .addComponent(jLabel6, javax.swing.GroupLayout.PREFERRED_SIZE, 63, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addGap(40, 40, 40)))
                .addContainerGap(963, Short.MAX_VALUE))
        );
        jPanel3Layout.setVerticalGroup(
            jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(jPanel3Layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING, false)
                    .addGroup(jPanel3Layout.createSequentialGroup()
                        .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(jPanel4, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addGroup(jPanel3Layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(2, 2, 2)
                                .addComponent(performance)
                                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                                .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 421, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(jPanel2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addComponent(jPanel5, javax.swing.GroupLayout.PREFERRED_SIZE, 0, Short.MAX_VALUE))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 46, Short.MAX_VALUE)
                .addGroup(jPanel3Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(idRecrutement, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(commentaireAjout, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ligneSelectionner)
                    .addComponent(commentaireRecru, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(commentaireModif, javax.swing.GroupLayout.PREFERRED_SIZE, 27, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(exp)
                    .addComponent(niv)
                    .addComponent(txtSpeci)
                    .addComponent(txtFrns)
                    .addComponent(txtAngls)
                    .addComponent(txtDiplo)))
        );

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addComponent(jPanel3, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(0, 0, Short.MAX_VALUE))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addComponent(jPanel3, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void jTable1MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseClicked
        int i = jTable1.getRowCount();
        if(i > 0){
           gc.image( String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(),0)), image , Format );
           gc.deplacement(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(),0)), combo , id ,  nom , prenom , dateNaiss , sexe , age , cin , adrs , tel,mail,etat);
           gp.deplacement(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(),0)), profil);
           gp.affichagePerfo(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(),0)), exp, niv, txtSpeci, txtFrns, txtAngls, txtDiplo, performance);     
           ligneSelect();imagePerfo();
          
        }
        
    }//GEN-LAST:event_jTable1MouseClicked

    private void jLabel28MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel28MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel28MouseClicked

    private void rechercheKeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyReleased
        gestionCandidat gc = new gestionCandidat();
        if(!"".equals(recherche.getText())){
          gc.rechercher(jTable1, idRecrutement.getText() , recherche.getText());
        }else{
            gc.actualiser(jTable1 , idRecrutement);
        }
    }//GEN-LAST:event_rechercheKeyReleased

    private void rechercheKeyTyped(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyTyped
        // TODO add your handling code here:

    }//GEN-LAST:event_rechercheKeyTyped

    private void rechercheKeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_rechercheKeyPressed
        // TODO add your handling code here:
    }//GEN-LAST:event_rechercheKeyPressed

    private void jLabel29MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel29MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel29MouseClicked
    
    private void jPanel5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseEntered
         actualiserModification();actualiserAjout();actualiserApresRecruter();
    }//GEN-LAST:event_jPanel5MouseEntered

    private void jLabel2MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel2MouseClicked
        if("".equals(id.getText())){
            JOptionPane.showMessageDialog(null,"Aucune donées selectionnées");
        }else{
            commentaireModif.setText("B");
             CandidatEditer c = new CandidatEditer();
             //c.idCandidat(id.getText());
             c.getDonnees(idRecrutement.getText(),combo.getSelectedItem().toString(),id.getText(), nom.getText(),prenom.getText() , dateNaiss.getText(), sexe.getText(), age.getText(), cin.getText(), adrs.getText(), tel.getText() , mail.getText() , profil.getText(), etat.getText());
             c.getDonneesPerfo(exp.getText() , niv.getText() , txtSpeci.getText() , txtFrns.getText() , txtAngls.getText() , txtDiplo.getText());
             c.setVisible(true);
        }

    }//GEN-LAST:event_jLabel2MouseClicked

    private void jLabel3MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel3MouseClicked
            gestionCandidat gc = new gestionCandidat();
            gestionProfil gp = new gestionProfil();
            if("recruté".equals(etat.getText())){
                JOptionPane.showMessageDialog(null,"Suppreéssion impossible!! Le candidat est déjà récruté");
            }else{
                if(!"".equals(id.getText())){
                    if(JOptionPane.showConfirmDialog (null,"Voulez vous vraiment suppriment? ","Confiramtion",
                        JOptionPane.YES_NO_OPTION)== JOptionPane.OK_OPTION){
                        gc.delete(id.getText());
                        gp.delete(id.getText());
                        gp.deletePerfo(id.getText());
                        gc.actualiser(jTable1 , idRecrutement);
                        if(jTable1.getRowCount()> 0){
                            selectionnerPremiereLigne();
                        }else{
                            vider();
                        }

                    }
                }else{
                     JOptionPane.showMessageDialog(null,"Aucune donées selectionnées");
                }
            }
            
    }//GEN-LAST:event_jLabel3MouseClicked

    private void jLabel4MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel4MouseClicked
        CandidatForm c = new CandidatForm();
        c.show();
        c.recruperer(combo.getSelectedItem().toString() , idRecrutement.getText());
        commentaireAjout.setText("B");
    }//GEN-LAST:event_jLabel4MouseClicked

    private void commentaireModifMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_commentaireModifMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_commentaireModifMouseClicked

    private void jPanel3MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel3MouseEntered
         actualiserModification();actualiserAjout();
    }//GEN-LAST:event_jPanel3MouseEntered

    private void jPanel6MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel6MouseEntered
         actualiserModification();actualiserAjout();actualiserApresRecruter();
    }//GEN-LAST:event_jPanel6MouseEntered

    private void jLabel32MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel32MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel32MouseClicked

    private void nomMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_nomMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_nomMouseClicked

    private void jLabel34MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel34MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel34MouseClicked

    private void prenomMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_prenomMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_prenomMouseClicked

    private void jLabel36MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel36MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel36MouseClicked

    private void dateNaissMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_dateNaissMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_dateNaissMouseClicked

    private void jLabel38MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel38MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel38MouseClicked

    private void sexeMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_sexeMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_sexeMouseClicked

    private void jLabel40MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel40MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel40MouseClicked

    private void ageMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_ageMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_ageMouseClicked

    private void jLabel42MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel42MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel42MouseClicked

    private void cinMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_cinMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_cinMouseClicked

    private void jLabel44MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel44MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel44MouseClicked

    private void adrsMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_adrsMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_adrsMouseClicked

    private void idRecrutementMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_idRecrutementMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_idRecrutementMouseClicked

    private void comboItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_comboItemStateChanged
      idRec();candidatAuRecrutement();selectionnerPremiereLigne();compte();
      if(jTable1.getRowCount()<=0){vider();}
      
    }//GEN-LAST:event_comboItemStateChanged

    private void formInternalFrameActivated(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameActivated
        
    }//GEN-LAST:event_formInternalFrameActivated

    private void formMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_formMouseEntered
        actualiserModification();actualiserAjout();
    }//GEN-LAST:event_formMouseEntered

    private void jPanel2MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel2MouseEntered
         actualiserModification();actualiserAjout();
    }//GEN-LAST:event_jPanel2MouseEntered

    private void jTable1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jTable1MouseEntered
        actualiserModification();actualiserAjout();actualiserApresRecruter();
    }//GEN-LAST:event_jTable1MouseEntered

    private void telMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_telMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_telMouseClicked

    private void jPanel1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel1MouseEntered
        actualiserModification();actualiserAjout();
    }//GEN-LAST:event_jPanel1MouseEntered

    private void jLabel33MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel33MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel33MouseClicked

    private void idMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_idMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_idMouseClicked

    private void jScrollPane1MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jScrollPane1MouseEntered
         actualiserModification();actualiserAjout();actualiserApresRecruter();
    }//GEN-LAST:event_jScrollPane1MouseEntered

    private void jTable1KeyPressed(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyPressed
        
    }//GEN-LAST:event_jTable1KeyPressed

    private void jTable1KeyReleased(java.awt.event.KeyEvent evt) {//GEN-FIRST:event_jTable1KeyReleased
        int i = jTable1.getRowCount();
        if(i > 0){
            gc.image(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(),0)) , image, Format);
            gc.deplacement(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(),0)), combo ,id ,  nom , prenom , dateNaiss , sexe , age , cin , adrs , tel, mail , etat);
            gp.affichagePerfo(String.valueOf(jTable1.getValueAt(jTable1.getSelectedRow(),0)), exp, niv, txtSpeci, txtFrns, txtAngls, txtDiplo, performance);
            
            ligneSelect();
            imagePerfo();
        }
    }//GEN-LAST:event_jTable1KeyReleased

    private void jLabel30MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel30MouseClicked
        imprimer();
    }//GEN-LAST:event_jLabel30MouseClicked

    private void jLabel30MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel30MouseEntered
        jLabel30.setForeground(new java.awt.Color(0,102,204));
        jLabel30.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(0,102,204)));
    }//GEN-LAST:event_jLabel30MouseEntered

    private void jLabel30MouseExited(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel30MouseExited
        jLabel30.setForeground(new java.awt.Color(102,102,102));
        jLabel30.setBorder(javax.swing.BorderFactory.createMatteBorder(0, 0, 1, 0, new java.awt.Color(255,255,255)));
    }//GEN-LAST:event_jLabel30MouseExited

    private void jPanel4MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel4MouseEntered
        actualiserModification();actualiserAjout();
    }//GEN-LAST:event_jPanel4MouseEntered

    private void idRecMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_idRecMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_idRecMouseClicked

    private void jLabel46MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel46MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel46MouseClicked

    private void jLabel47MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel47MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jLabel47MouseClicked

    private void mailMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mailMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_mailMouseClicked

    private void formInternalFrameOpened(javax.swing.event.InternalFrameEvent evt) {//GEN-FIRST:event_formInternalFrameOpened
      
    }//GEN-LAST:event_formInternalFrameOpened

    private void commentaireAjoutMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_commentaireAjoutMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_commentaireAjoutMouseClicked

    private void btnRecMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_btnRecMouseClicked
        if("recruté".equals(etat.getText())){
            JOptionPane.showMessageDialog(null,"ce candidat est deja récruté");
        }else{
            commentaireRecru.setText("B");
            recruterValidation rv = new recruterValidation();
            rv.getDonnees(id.getText() , combo.getSelectedItem().toString() , nom.getText() , prenom.getText() , idRecrutement.getText());
            rv.setVisible(true);
        }
       
    }//GEN-LAST:event_btnRecMouseClicked

    private void commentaireRecruMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_commentaireRecruMouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_commentaireRecruMouseClicked

    private void profilMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_profilMouseEntered
        // TODO add your handling code here:
    }//GEN-LAST:event_profilMouseEntered

    private void jPanel5MouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jPanel5MouseClicked
        // TODO add your handling code here:
    }//GEN-LAST:event_jPanel5MouseClicked

    private void effectifMouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_effectifMouseEntered
        actualiserApresRecruter();
    }//GEN-LAST:event_effectifMouseEntered

    private void jLabel5MouseEntered(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_jLabel5MouseEntered
        actualiserApresRecruter();
    }//GEN-LAST:event_jLabel5MouseEntered


    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JLabel adrs;
    private javax.swing.JLabel age;
    private javax.swing.JLabel btnRec;
    private javax.swing.JLabel cin;
    private javax.swing.JComboBox<String> combo;
    private javax.swing.JLabel commentaireAjout;
    private javax.swing.JLabel commentaireModif;
    private javax.swing.JLabel commentaireRecru;
    private javax.swing.JLabel dateNaiss;
    private javax.swing.JLabel effectif;
    private javax.swing.JLabel etat;
    private javax.swing.JLabel exp;
    private javax.swing.JLabel id;
    private javax.swing.JLabel idRec;
    private javax.swing.JLabel idRecApresModification;
    private javax.swing.JLabel idRecrutement;
    private javax.swing.JLabel image;
    private javax.swing.JDesktopPane jDesktopPane1;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel28;
    private javax.swing.JLabel jLabel29;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel30;
    private javax.swing.JLabel jLabel32;
    private javax.swing.JLabel jLabel33;
    private javax.swing.JLabel jLabel34;
    private javax.swing.JLabel jLabel36;
    private javax.swing.JLabel jLabel38;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel40;
    private javax.swing.JLabel jLabel42;
    private javax.swing.JLabel jLabel44;
    private javax.swing.JLabel jLabel46;
    private javax.swing.JLabel jLabel47;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JPanel jPanel2;
    private javax.swing.JPanel jPanel3;
    private javax.swing.JPanel jPanel4;
    private javax.swing.JPanel jPanel5;
    private javax.swing.JPanel jPanel6;
    private javax.swing.JScrollPane jScrollPane1;
    private javax.swing.JScrollPane jScrollPane2;
    private javax.swing.JScrollPane jScrollPane3;
    private javax.swing.JTable jTable1;
    private javax.swing.JLabel ligneSelectionner;
    private javax.swing.JLabel mail;
    private javax.swing.JLabel niv;
    private javax.swing.JLabel nom;
    private javax.swing.JLabel performance;
    private javax.swing.JLabel prenom;
    private javax.swing.JTextArea profil;
    private javax.swing.JTextField recherche;
    private javax.swing.JLabel sexe;
    private javax.swing.JLabel tel;
    private javax.swing.JLabel txtAngls;
    private javax.swing.JLabel txtDiplo;
    private javax.swing.JLabel txtFrns;
    private javax.swing.JLabel txtSpeci;
    // End of variables declaration//GEN-END:variables
}
