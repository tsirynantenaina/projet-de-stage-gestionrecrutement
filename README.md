# Gestion de recrument du personnel (JAVA Desktop)
Le projet est deidé est au bureau communal de Fianarantsoa (CUF)

## But de l'application
Ce projet permet de :
* Améliorer la mode de saisi des avis de recrutement : la saisie actuelle se fait avec une machine à écrire.
* Afficher les candidats dans un recrutement.
* Connaitre en un seul coup d’œil la performance d’un candidat.
* Afficher graphiquement le taux de recrutement annuel
* Imprimer automatiquement les avis de recrutement


## Prérequis et installation
* L'application se lance sur NetBeans
* Vous devez installer Netbeans sur votre Machine



## Base de donnees 
* Importer "cuf.sql" dans votre base 

## Commant lancer l'application?
* Ouvrir le dossir sur le NetBeans
* lancer l'application

## Authentification Par defaut
* Utilisateur:
```
admin

```
* Mot de passe:
```
admin

```
